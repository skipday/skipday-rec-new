/*
package com.skipday.sdrec.lite.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

*/
/**
 * Created by Jaga My Priera on 18/01/2015.
 *//*

public class DBAdapter {
    public static final int BY_NAME_INC = 0;
    public static final int BY_DATE_INC = 2;
    public static final int BY_DATE_DEC = 3;
    public static final int BY_SIZE_INC = 4;
    public static final int BY_SIZE_DEC = 5;
    public static final int BY_NAME_DEC = 1;
    @NotNull
    public static final String
            KEY_ROWID = "_id";
    @NotNull
    public static final String KEY_NAME = "name";
    @NotNull
    public static final String KEY_LOCATION = "location";
    @NotNull
    public static final String KEY_SIZE = "sizes";
    @NotNull
    public static final String KEY_CREATED = "created";
    @NotNull
    public static final String DATABASE_NAME = "Listing";
    @NotNull
    public static final String DATABASE_TABLE = "mysound";
    @NotNull
    public static final String TAG = "DBAdapter Class";
    @NotNull
    public static final String CREATE_DATABASE = "create table mysound (" +
                    "_id integer primary key autoincrement, " +
                    "name text not null, location text not null, " +
                    "sizes long not null, created long not null )";
    private static int DATABASE_VERSION = 1;
    private final Context context;
    private SQLiteDatabase db;
    private final DatabaseHelper DBHelper;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);

    }

    @NotNull
    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        DBHelper.close();
    }

    public long insertTitle(String name, String location, long sizes, long created) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_LOCATION, location);
        initialValues.put(KEY_SIZE, sizes);
        initialValues.put(KEY_CREATED, created);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    public boolean deletesongs(long rowId) {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;

    }

    public Cursor getMaxSize() {
        return db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_NAME, KEY_LOCATION, KEY_SIZE}, null, null, null, null, KEY_ROWID + " DESC", "1");
    }

    public Cursor getAllColumn(int SORTBY) {
        String name = null, type = null;
        switch (SORTBY) {
            case BY_NAME_INC: {
                name = KEY_NAME + " COLLATE NOCASE ";
                type = "ASC";
            }
            break;
            case BY_DATE_INC: {
                name = KEY_CREATED;
                type = " ASC";
            }
            break;
            case BY_SIZE_INC: {
                name = KEY_SIZE;
                type = " ASC";
            }
            break;
            case BY_NAME_DEC: {
                name = KEY_NAME + " COLLATE NOCASE ";
                type = "DESC";
            }
            break;
            case BY_DATE_DEC: {
                name = KEY_CREATED;
                type = " DESC";
            }
            break;
            case BY_SIZE_DEC: {
                name = KEY_SIZE;
                type = " DESC";
            }
            break;
        }
        return db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_NAME, KEY_LOCATION, KEY_SIZE, KEY_CREATED}, KEY_ROWID, null, null, null, name + type, null);
    }

    public Cursor getRowIDoForSongs(String song) {

        return db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_NAME}, KEY_NAME + " like '%" + song + "%'", null, null, null, null, null);
    }

    public boolean updatesongs(long rowId, String name, String location, long sizes, long created) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_LOCATION, location);
        contentValues.put(KEY_SIZE, sizes);
        contentValues.put(KEY_CREATED, created);
        return db.update(DATABASE_TABLE, contentValues, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public boolean isExist(String song) {
        boolean ada = true;
        Cursor cursor;// = db.rawQuery("SELECT "+KEY_NAME+" FROM " + DATABASE_TABLE + " WHERE " + KEY_NAME + " LIKE '%" + song + "%'", null);
        cursor=db.query(true,DATABASE_TABLE,new String[]{KEY_NAME},KEY_NAME + " LIKE '%" + song + "%'",null,null,null,null,null);
        ada = cursor != null && cursor.moveToFirst();
        return ada;
    }

    public boolean isEmpty() {
        boolean kosong = true;
        String count = "SELECT COUNT (*) FROM " + DATABASE_TABLE;
        Cursor cursor = db.rawQuery(count, null);
        kosong = !(cursor != null && cursor.moveToFirst());
        //kosong=(cursor.getInt(0)==0);
        cursor.close();
        return kosong;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(@NotNull SQLiteDatabase db) {
            db.execSQL(CREATE_DATABASE);
        }

        @Override
        public void onUpgrade(@NotNull SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + " will destroy old data");
            db.execSQL("DROP TABLE IF EXIST mysound");
            onCreate(db);
        }
    }
}
*/
