
package com.skipday.sdrec.lite.CustomView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import org.jetbrains.annotations.NotNull;

public class AnimatedView extends View {
    @NotNull
    private final
    Paint mPaint = new Paint();
    @NotNull
    private final
    Paint mPaint2 = new Paint();
    @NotNull
    private final
    Paint mPaint3 = new Paint();
    @NotNull
    private final
    Paint mPaint4 = new Paint();
    @NotNull
    private final
    Paint mPaint5 = new Paint();
    @NotNull
    private final
    Paint mPaint6 = new Paint();
    private final RectF rectf = new RectF();
    private final RectF rectf2 = new RectF();
    private final RectF rectf3 = new RectF();
    private final RectF rectf4 = new RectF();
    private final RectF rectf5 = new RectF();
    private final RectF rectf6 = new RectF();
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int RADIUS;
    private final float density;


    public AnimatedView(@NotNull Context context, AttributeSet attrs) {
        super(context, attrs);
        density = getResources().getDisplayMetrics().density;

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(48);
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaint2.setStyle(Paint.Style.STROKE);
        mPaint2.setColor(Color.WHITE);
        mPaint2.setAlpha(48);
        mPaint2.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaint3.setStyle(Paint.Style.STROKE);
        mPaint3.setColor(Color.WHITE);
        mPaint3.setAlpha(48);
        mPaint3.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaint4.setStyle(Paint.Style.STROKE);
        mPaint4.setColor(Color.WHITE);
        mPaint4.setAlpha(48);
        mPaint4.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaint5.setStyle(Paint.Style.STROKE);
        mPaint5.setColor(Color.WHITE);
        mPaint5.setAlpha(48);
        mPaint5.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaint6.setStyle(Paint.Style.STROKE);
        mPaint6.setColor(Color.WHITE);
        mPaint6.setAlpha(48);
        mPaint6.setFlags(Paint.ANTI_ALIAS_FLAG);

    }

    public void setRADIUS(int radius) {
        this.RADIUS = radius;
        invalidate();
    }

    public void setLoudness(int loudnessI, int loudnessJ, int loudnessK, int loudnessL, int loudnessM, int loudnessN) {
        i = loudnessI;
        j = loudnessJ;
        k = loudnessK;
        l = loudnessL;
        m = loudnessM;
        n = loudnessN;
        invalidate();
        requestLayout();
    }

    protected void onDraw(@NotNull Canvas canvas) {
        int cx = getWidth() / 2; //x-koordinat tengah layar di x
        int cy = getHeight() / 2; //y-koordinat tengah layar di y
        mPaint.setStrokeWidth(i * density);
        rectf.set(cx - (RADIUS) - (i / 2 * density), cy - (RADIUS) - (i / 2 * density), cx + (RADIUS) + (i / 2 * density), cy + (RADIUS) + (i / 2 * density));


        mPaint2.setStrokeWidth(j * density);
        rectf2.set(cx - (RADIUS) - (j / 2 * density), cy - (RADIUS) - (j / 2 * density), cx + (RADIUS) + (j / 2 * density), cy + (RADIUS) + (j / 2 * density));

        mPaint3.setStrokeWidth(k * density);
        rectf3.set(cx - (RADIUS) - (k / 2 * density), cy - (RADIUS) - (k / 2 * density), cx + (RADIUS) + (k / 2 * density), cy + (RADIUS) + (k / 2 * density));

        mPaint4.setStrokeWidth(l * density);
        rectf4.set(cx - (RADIUS) - (l / 2 * density), cy - (RADIUS) - (l / 2 * density), cx + (RADIUS) + (l / 2 * density), cy + (RADIUS) + (l / 2 * density));

        mPaint5.setStrokeWidth(m * density);
        rectf5.set(cx - (RADIUS) - (m / 2 * density), cy - (RADIUS) - (m / 2 * density), cx + (RADIUS) + (m / 2 * density), cy + (RADIUS) + (m / 2 * density));

        mPaint6.setStrokeWidth(n * density);
        rectf6.set(cx - (RADIUS) - (n / 2 * density), cy - (RADIUS) - (n / 2 * density), cx + (RADIUS) + (n / 2 * density), cy + (RADIUS) + (n / 2 * density));

        canvas.drawArc(rectf5, 300, 40, false, mPaint5);
        canvas.drawArc(rectf2, 320, 20, false, mPaint2);
        canvas.drawArc(rectf, 337, 40, false, mPaint);
        canvas.drawArc(rectf2, 180, 35, false, mPaint2);
        canvas.drawArc(rectf4, 140, 15, false, mPaint4);
        canvas.drawArc(rectf3, 260, 34, false, mPaint3);
        canvas.drawArc(rectf6, 240, 70, false, mPaint6);
        canvas.drawArc(rectf3, 120, 120, false, mPaint3);
        canvas.drawArc(rectf, 90, 12, false, mPaint);
        canvas.drawArc(rectf3, 80, 120, false, mPaint3);
        canvas.drawArc(rectf5, 0, 84, false, mPaint5);
    }

}

