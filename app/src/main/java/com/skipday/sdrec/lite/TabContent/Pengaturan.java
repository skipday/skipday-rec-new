package com.skipday.sdrec.lite.TabContent;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.skipday.sdrec.lite.R;
import com.skipday.sdrec.lite.TabContent.PengaturanContent.CropOption;
import com.skipday.sdrec.lite.TabContent.PengaturanContent.CropOptionAdapter;
import com.skipday.sdrec.lite.TabContent.PengaturanContent.Donating;
import com.skipday.sdrec.lite.TabContent.PengaturanContent.SetSavePath;
import com.skipday.sdrec.lite.TabContent.RecordingListContent.dialog;
import com.skipday.sdrec.lite.util.IabHelper;
import com.suredigit.inappfeedback.FeedbackDialog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



@SuppressWarnings("WeakerAccess")
public class Pengaturan extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String BITRATE_PREF = "bitratePref";
    public static final String QUALITY_PREF = "qualityPref";
    static final String DONATE = "donate";
    static final int RC_REQUEST = 10001;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    public dialog dlg;
    String value;
    IabHelper mHelper;
    int flag;
    @NotNull
    String TAG = "pengaturan";
    @Nullable
    File g = null;
    TextView seekInfo;
    String base64EncodedPublicKey;
    SharedPreferences prefs;
    private SeekBar seekquality;
    private FeedbackDialog feedBack;
    @Nullable
    private Uri mImageCaptureUri;
    private SdrecMP3 sdrecMP3;

    private static boolean deleteDirectory(@NotNull File path) {

        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    @Override
    public void onResume() {
        super.onResume();

        /*String M_BACKGROUND = prefs.getString("background", "fgh");
        Boolean ata = prefs.getBoolean("ata", false);
        int a = prefs.getInt("alpha", random(255, 0));
        int r = prefs.getInt("red", random(255, 0));
        int g = prefs.getInt("green", random(255, 0));
        int b = prefs.getInt("blue", random(255, 0));
        if (ata) {
            if (M_BACKGROUND.equals("SKIPDAY")) {
                findViewById(R.id.preferences).setBackgroundColor(Color.argb(a, r, g, b));
            } else {
                Resources res = getResources();
                Bitmap bitmap = BitmapFactory.decodeFile(M_BACKGROUND);
                BitmapDrawable bd = new BitmapDrawable(res, bitmap);
                findViewById(R.id.preferences).setBackgroundDrawable(bd);
            }

        } else {
            findViewById(R.id.preferences).setBackgroundColor(Color.argb(0, 0, 0, 0));
        }*/
    }

    void complain(String message) {
        Log.e(TAG, "**** TrivialDrive Error: " + message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }
    private Donating p;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            p=new Donating(Pengaturan.this);
        }catch (Exception e){

        }
        addPreferencesFromResource(R.xml.pengaturan);
        setContentView(R.layout.pengaturan);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        sdrecMP3 = new SdrecMP3();
        Preference preference = findPreference("directory");
        Preference back = findPreference("background");
        Preference bitrate = findPreference("bitratePref");
        Preference ownPref = findPreference("ownPref");
        Preference about = findPreference("about");
        Preference donate = findPreference("donate");
        dlg = new dialog();
        final SharedPreferences.Editor editor = prefs.edit();
        assert bitrate != null;
        assert back != null;
        assert preference != null;
        assert ownPref != null;
        assert about != null;
        donate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                try{
                    p.transaction();
                    new Donating(Pengaturan.this);
                }catch (Exception e){
                    Toast.makeText(Pengaturan.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });
        about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                aboutDialog();
                return false;
            }
        });
        ownPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                dlg.rename("Enter your own text here", Pengaturan.this, "PREFIX");
                return false;
            }
        });
        bitrate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                bitrateDlg();
                return true;
            }
        });
        /*back.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setBitmap();
                return false;
            }
        });*/

        final CheckBoxPreference namePr = (CheckBoxPreference) findPreference("askNamePref");
        final CheckBoxPreference ata = (CheckBoxPreference) findPreference("ataPref");

        assert ata != null;
        assert namePr != null;
        /*ata.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String M_BACKGROUND = prefs.getString("background", "fgh");
                if (ata.isChecked()) {
                    editor.putBoolean("ata", true);
                    editor.apply();
                    int a = prefs.getInt("alpha", random(255, 0));
                    int r = prefs.getInt("red", random(255, 0));
                    int g = prefs.getInt("green", random(255, 0));
                    int b = prefs.getInt("blue", random(255, 0));

                    if (M_BACKGROUND.equals("SKIPDAY")) {
                        findViewById(R.id.preferences).setBackgroundColor(Color.argb(a, r, g, b));
                    } else {
                        Resources res = getResources();
                        Bitmap bitmap = BitmapFactory.decodeFile(M_BACKGROUND);
                        BitmapDrawable bd = new BitmapDrawable(res, bitmap);
                        findViewById(R.id.preferences).setBackgroundDrawable(bd);
                    }
                } else {
                    editor.putBoolean("ata", false);
                    editor.apply();
                    findViewById(R.id.preferences).setBackgroundColor(Color.argb(255, 0, 0, 0));
                }
                return false;
            }
        });*/
        namePr.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (namePr.isChecked()) {
                    editor.putBoolean("askName", true);
                    editor.apply();
                } else {
                    editor.putBoolean("askName", false);
                    editor.apply();
                }
                return false;
            }
        });
        final CheckBoxPreference h = (CheckBoxPreference) findPreference("mediaPref");
        assert h != null;
        h.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (!h.isChecked()) {
                    tracking("Berhasil");
                    File pustaka = new File(prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec") + "/.nomedia");
                    try {
                        pustaka.createNewFile();

                        toastTampil("Removed from media library");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    File pustaka = new File(prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec") + "/.nomedia");
                    if (pustaka.exists()) {
                        pustaka.delete();
                        toastTampil("Succsessfully added to media library");
                    }
                }
                return true;
            }
        });

        preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String s = Environment.getExternalStorageState();
                Intent intent = new Intent(Pengaturan.this, SetSavePath.class);
                if (s.equals(Environment.MEDIA_MOUNTED)) {
                    startActivityForResult(intent, SetSavePath.PICK_DIRECTORY);
                } else sdrecMP3.dialog(0, "Storage not ready", Pengaturan.this);
                return true;
            }
        });
        Preference folder = findPreference("directory");
        folder.setSummary(prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec"));
    }

    private void toastTampil(String pesan) {
        Toast.makeText(Pengaturan.this, pesan, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @NotNull Intent data) {
        if (resultCode != -1) return;
        if (requestCode == SetSavePath.PICK_DIRECTORY && resultCode == -1) {
            Bundle extras = data.getExtras();
            String path = (String) extras.get(SetSavePath.CHOSEN_DIRECTORY);

            Preference folder = findPreference("directory");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("directory", path);
            folder.setSummary(path);
            editor.apply();

        }

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                tracking("PICK_FROM_CAMERA");
                doCrop();

                break;
            case PICK_FROM_FILE:
                mImageCaptureUri = data.getData();
                tracking("PICK_FROM_FILE");
                doCrop();
                break;
            case CROP_FROM_CAMERA:
                if (flag == 1) {


                    assert g != null;
                    if (g.exists()) g.delete();
                    tracking(g.toString());
                }
                tracking("CROP_FROM_CAMERA");
                break;

        }
    }

    private void tracking(String pesan) {
        Log.d("Pengaturan", pesan);
    }

    public int random(int max, int min) {
        int a;
        a = (int) (Math.random() * (max - min));
        return a;
    }

    @NotNull
    private Dialog bitrateDlg() {
        final SharedPreferences.Editor editor = prefs.edit();
        LayoutInflater inflater = LayoutInflater.from(Pengaturan.this);
        View view = inflater.inflate(R.layout.dialog_seek, null);
        assert view != null;
        final SeekBar seekQuality = (SeekBar) view.findViewById(R.id.quality);
        final TextView et = (TextView) view.findViewById(R.id.qualityvalue);
        final TextView kbps = (TextView) view.findViewById(R.id.kbps);
        final TextView ket = (TextView) view.findViewById(R.id.ket);
        kbps.setText("kbps");
        kbps.setVisibility(View.VISIBLE);
        seekQuality.setMax(7);
        Integer currentBitrate = Integer.parseInt(prefs.getString("bitratePref", "128"));
        et.setText("" + currentBitrate);
        switch (currentBitrate) {
            case 320: {
                seekQuality.setProgress(7);
                ket.setText("2.4 MB/min");
            }
            break;
            case 256: {
                seekQuality.setProgress(6);
                ket.setText("1.9 MB/min");
            }
            break;
            case 192: {
                seekQuality.setProgress(5);
                ket.setText("1.5 MB/min");
            }
            break;
            case 160: {
                seekQuality.setProgress(4);
                ket.setText("1.2 MB/min");
            }
            break;
            case 128: {
                seekQuality.setProgress(3);
                ket.setText("1.0 MB/min, similar to CD quality");
            }
            break;
            case 96: {
                seekQuality.setProgress(2);
                ket.setText("0.7 MB/min, default");
            }
            break;
            case 64: {
                seekQuality.setProgress(1);
                ket.setText("0.5 MB/min");
            }
            break;
            case 32: {
                seekQuality.setProgress(0);
                ket.setText("0.24 MB/min,for speeches");
            }
            break;
            default: {
                seekQuality.setProgress(2);
                ket.setText("0.7 MB/min, default");
            }
            break;
        }
        seekQuality.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                et.setText(""+seekQuality.getProgress());

                int a = seekQuality.getProgress();
                switch (a) {
                    case 0: {
                        et.setText("" + 32);
                        ket.setText("0.24 MB/min,for speeches");
                    }
                    break;
                    case 1: {
                        et.setText("" + 64);
                        ket.setText("0.5 MB/min");
                    }
                    break;
                    case 2: {
                        et.setText("" + 96);
                        ket.setText("0.7 MB/min, default");
                    }
                    break;
                    case 3: {
                        et.setText("" + 128);
                        ket.setText("1.0 MB/min, similar to CD quality");
                    }
                    break;
                    case 4: {
                        et.setText("" + 160);
                        ket.setText("1.2 MB/min");
                    }
                    break;
                    case 5: {
                        et.setText("" + 192);
                        ket.setText("1.5 MB/min");
                    }
                    break;
                    case 6: {
                        et.setText("" + 256);
                        ket.setText("1.9 MB/min");
                    }
                    break;
                    case 7: {
                        et.setText("" + 320);
                        ket.setText("2.4 MB/min");
                    }
                    break;
                    default: {
                        et.setText("" + 96);
                        ket.setText("0.7 MB/min, default");
                    }
                    break;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        return new AlertDialog.Builder(Pengaturan.this)
                .setTitle("Change bitrate").setView(view)
                .setCancelable(false)

                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putString("bitratePref", String.valueOf(et.getText()));
                                editor.apply();
                            }
                        }
                )
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNeutralButton("Default value", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        seekQuality.setProgress(2);
                        et.setText("" + 96);
                        ket.setText("0.7 MB/min, default");
                        editor.putString("bitratePref", "96");
                        editor.apply();

                    }
                })
                .show();
    }

    @NotNull
    private Dialog setBitmap() {
        final String[] items = new String[]{"Take from camera", "Select from gallery", "Random color", "I don't like any"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Pengaturan.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(Pengaturan.this);
        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { //pick from camera
                if (item == 0) {
                    flag = 1;
                    File f = null;
                    f = new File(Environment.getExternalStorageDirectory() + "/Skipday_Rec_cropped");
                    deleteDirectory(new File(Environment.getExternalStorageDirectory() + "/Skipday_Rec_cropped"));
                    f.mkdir();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    g = new File(Environment.getExternalStorageDirectory(),
                            "tmp.jpg");
                    mImageCaptureUri = Uri.fromFile(g);
                    tracking("!%$!^#&*($%$$$" + g);
                    tracking("!%$!^#&*($%$$$" + mImageCaptureUri);
                    intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

                    try {
                        intent.putExtra("return-data", false);
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                if (item == 1) { //pick from file
                    flag = 2;
                    Intent intent = new Intent();
                    intent.setType("image");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                }
                if (item == 2) {
                    SharedPreferences.Editor editor = prefs.edit();
                    int a = prefs.getInt("alpha", random(255, 0));
                    int r = prefs.getInt("red", random(255, 0));
                    int g = prefs.getInt("green", random(255, 0));
                    int b = prefs.getInt("blue", random(255, 0));
                    editor.putInt("alphaL", a);
                    editor.putInt("redL", r);
                    editor.putInt("greenL", g);
                    editor.putInt("blueL", b);
                    editor.putInt("alpha", random(255, 0));
                    editor.putInt("red", random(255, 0));
                    editor.putInt("green", random(255, 0));
                    editor.putInt("blue", random(255, 0));
                    editor.putString("background", "SKIPDAY");
                    editor.apply();
                    toastTampil("Random color applied");
                }
                if (item == 3) {
                    SharedPreferences.Editor editor = prefs.edit();
                    int a = prefs.getInt("alpha", random(255, 0));
                    int r = prefs.getInt("red", random(255, 0));
                    int g = prefs.getInt("green", random(255, 0));
                    int b = prefs.getInt("blue", random(255, 0));
                    editor.putInt("alphaL", a);
                    editor.putInt("redL", r);
                    editor.putInt("greenL", g);
                    editor.putInt("blueL", b);
                    editor.putInt("alpha", 0);
                    editor.putInt("red", 0);
                    editor.putInt("green", 0);
                    editor.putInt("blue", 0);
                    editor.putString("background", "SKIPDAY");
                    editor.apply();
                    toastTampil("You should like this one");
                }
            }
        });
        return builder.show();
    }

    private void doCrop() {
        Display display = Pengaturan.this.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        tracking(width + " " + height);
        SharedPreferences.Editor editor = prefs.edit();
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        File f, g = null;
        f = new File(Environment.getExternalStorageDirectory() + "/SDRec_cropped");
        deleteDirectory(new File(Environment.getExternalStorageDirectory() + "/SDRec_cropped"));
        f.mkdir();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image");
        List<ResolveInfo> list = Pengaturan.this.getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(Pengaturan.this, "Can not find image crop app", Toast.LENGTH_SHORT).show();

        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("scale", false);
            intent.putExtra("crop", true);
            g = new File(f + "/skipday.jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(g));
            intent.putExtra("output", Uri.fromFile(g));
            intent.putExtra("return-data", false);
            editor.putString("background", g.toString());
            editor.apply();
            tracking(g.toString());
            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                startActivityForResult(i, CROP_FROM_CAMERA);

            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();
                    co.title = Pengaturan.this.getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = Pengaturan.this.getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(Pengaturan.this, cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(Pengaturan.this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROP_FROM_CAMERA);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (mImageCaptureUri != null) {
                            Pengaturan.this.getContentResolver().delete(mImageCaptureUri, null, null);
                            mImageCaptureUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, @NotNull String key) {
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void aboutDialog() {
        Dialog about = new Dialog(Pengaturan.this);
        about.requestWindowFeature(Window.FEATURE_NO_TITLE);
        about.setContentView(R.layout.about);
        about.findViewById(R.id.aboutly).setBackgroundColor(Color.argb(255, 23, 23, 23));
        PackageManager pm = Pengaturan.this.getPackageManager();
        assert pm != null;
        ApplicationInfo installAt = null;
        PackageInfo versi = null;
        try {
            installAt = pm.getApplicationInfo(Pengaturan.this.getPackageName(), 0);
            versi = pm.getPackageInfo(Pengaturan.this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }
        assert installAt != null;
        assert versi != null;
        String appFile = installAt.sourceDir, namaVersi = versi.versionName, kodeVersi = String.valueOf(versi.versionCode);
        long installed = new File(appFile).lastModified();
        ImageButton likeUs = (ImageButton) about.findViewById(R.id.idLikeUs);
        likeUs.setMaxHeight(likeUs.getWidth());
        likeUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri=Uri.parse("market://details?id=com.skipday.sdrec.lite");
                Intent like=new Intent(Intent.ACTION_VIEW,uri);
                try {
                    startActivity(like);
                }catch (ActivityNotFoundException e){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("play.google.com/store/apps/details?id=com.skipday.sdrec.lite")));
                }

            }
        });
        TextView label = (TextView) about.findViewById(R.id.idTitle);
        TextView aboutText = (TextView) about.findViewById(R.id.aboutText);
        aboutText.setText(getString(R.string.about_text, "" + pm.getApplicationLabel(installAt)));
        TextView detailLabel = (TextView) about.findViewById(R.id.idDetailTitle);
        label.setText("" + pm.getApplicationLabel(installAt));
        detailLabel.setText("" + pm.getApplicationLabel(installAt));
        TextView installedDate = (TextView) about.findViewById(R.id.iDate);
        installedDate.setText("" + dateModif(installed));
        TextView version = (TextView) about.findViewById(R.id.version);
        version.setText("" + namaVersi + " (" + kodeVersi + ")");
        about.show();
    }

    public String dateModif(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy  HH:mm");
        return formatter.format(new Date(date));
    }
}

