package com.skipday.sdrec.lite;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.skipday.sdrec.lite.SlidingTabs.SlidingTabLayout;

public class MainActivity extends ActionBarActivity {
    SectionsPageAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPageAdapter(getSupportFragmentManager(), MainActivity.this);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        slidingTabLayout.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        slidingTabLayout.setFitsSystemWindows(true);
        slidingTabLayout.setViewPager(mViewPager);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setBackgroundColor(Color.parseColor("#480849"));
        slidingTabLayout.setBackgroundColor(Color.parseColor("#480849"));
        slidingTabLayout.setDividerColors(Color.TRANSPARENT);
        slidingTabLayout.setSelectedIndicatorColors(Color.WHITE);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
