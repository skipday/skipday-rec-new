package com.skipday.sdrec.lite.TabContent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.skipday.sdrec.lite.R;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jaga My Priera on 25/01/2015.
 */
public class SongInfo extends Activity {
    private EditText trackName, albumName, artistName, albumArtisName, genre;
    private String SOUND;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.audio_tag);
        trackName = (EditText) findViewById(R.id.tracknameinput);
        albumName = (EditText) findViewById(R.id.albumnameinput);
        artistName = (EditText) findViewById(R.id.artistnameinput);
        albumArtisName = (EditText) findViewById(R.id.albumartistnameinput);
        genre = (EditText) findViewById(R.id.genreinput);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
        Intent intent = getIntent();
        readTag(new File(folderRec + "/" + intent.getStringExtra("filename")));
        SOUND = folderRec + "/" + intent.getStringExtra("filename");
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    /*private void writeTag(File file){
        try {
            *//*MP3File mp3File = (MP3File) AudioFileIO.read(file);
            AbstractID3v2Tag tag=mp3File.getID3v2Tag();*//*
            AudioFile mp3File = AudioFileIO.read(file);
            Tag tag=mp3File.createDefaultTag();
            TagField tagField=new TagTextField() {
            }
            log(this,""+tag);
            tag.addField(tagField);
            tag.addField(FieldKey.TRACK);
            tag.addField(FieldKey.ALBUM);
            tag.addField(FieldKey.ARTIST, artistName.getText().toString());
            tag.addField(FieldKey.ALBUM_ARTIST, albumArtisName.getText().toString());
            tag.addField(FieldKey.GENRE, genre.getText().toString());

            try {
                mp3File.commit();
            } catch (CannotWriteException e) {
                toast(this, "CannotWriteException");
            }
        } catch (CannotReadException e) {
            toast(this,"CannotReadException" );
        } catch (IOException e) {
            toast(this, "IOException");
        } catch (TagException e) {
            toast(this, "TagException");
        } catch (ReadOnlyFileException e) {
            toast(this, "ReadOnlyFileException");
        } catch (InvalidAudioFrameException e) {
            toast(this, "InvalidAudioFrameException");
        }

    }*/
    private void readTag(File file) {
        try {
            MP3File mp3File = (MP3File) AudioFileIO.read(file);
            AbstractID3v2Tag tag = mp3File.getID3v2Tag();
            if (mp3File.hasID3v2Tag()) {

                trackName.setText((CharSequence) tag.getFirstField(FieldKey.TRACK));
                albumName.setText((CharSequence) tag.getFirstField(FieldKey.ALBUM));
                artistName.setText((CharSequence) tag.getFirstField(FieldKey.ARTIST));
                albumArtisName.setText((CharSequence) tag.getFirstField(FieldKey.ALBUM_ARTIST));
                genre.setText((CharSequence) tag.getFirstField(FieldKey.GENRE));
            } else {
                toast(this, "This file doesn't have mp3 tag");
            }

        } catch (CannotReadException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TagException e) {
            e.printStackTrace();
        } catch (ReadOnlyFileException e) {
            e.printStackTrace();
        } catch (InvalidAudioFrameException e) {
            e.printStackTrace();
        }


    }

    public void onButtonSongInfoClick(View button) {
        switch (button.getId()) {
            case R.id.committag: {
                toast(this, SOUND);
                //writeTag(new File(SOUND));
            }
            break;
            case R.id.cancelwritetag: {

            }
            break;
            case R.id.changeartwork: {

            }
            break;
        }
    }

    private void readInfo(File file) {
    }

    private void toast(Context tcontex, String pesan) {
        Toast.makeText(tcontex, pesan, Toast.LENGTH_SHORT).show();
    }

    private void log(Context context, String pesan) {
        Log.d("" + context, pesan);
    }
}
