package com.skipday.sdrec.lite.Services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.NoiseSuppressor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.skipday.sdrec.lite.Helper.CallDetectHelper;
import com.skipday.sdrec.lite.MainActivity;
import com.skipday.sdrec.lite.R;
import com.skipday.sdrec.lite.TabContent.Constant;
import com.skipday.sdrec.lite.lame.SimpleLame;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Jaga My Priera on 13/05/2014.
 */
public class RecordingService extends Service {
    public static final int MSG_REC_STARTED = 0;
    public static final int MSG_REC_STOPPED = 1;
    public static final int MSG_ERROR_GET_MIN_BUFFERSIZE = 2;
    public static final int MSG_ERROR_CREATE_FILE = 3;
    public static final int MSG_ERROR_REC_START = 4;
    public static final int MSG_ERROR_AUDIO_RECORD = 5;
    public static final int MSG_ERROR_AUDIO_ENCODE = 6;
    public static final int MSG_ERROR_WRITE_FILE = 7;
    public static final int MSG_ERROR_CLOSE_FILE = 8;
    public static final int REKAM = 0;
    public static final int BERHENTI = 1;
    public static final int JEDA = 2;

    static {
        System.loadLibrary("mp3lame");
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private final BroadcastReceiver boostValue = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            boostSet = intent.getIntExtra("boostValue", 0);
        }
    };
    public AudioRecord audioRecord;
    public String JUDUL;
    public int SAMPLE_RATE, BIT_RATE, QUALITY, MICSOURCE, GAIN, NULLAvoid, CHANNEL, FORMAT;
    int flagCDH;
    TelephonyManager tm;
    PhoneStateListener pl;
    Object CAMCORDER, DEFAULT, MIC;
    boolean mAllowRebind;
    double sum;
    int ampli;
    int noiseSet, boostSet;
    String size;
    private CallDetectHelper cdh;
    private boolean mIsRecording = false;
    private Handler mHandler;
    private short[] mBuffer;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        cdh = new CallDetectHelper(this);
        Notification note = new Notification(0, "Standby", System.currentTimeMillis());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        size = prefs.getString("directory", "/");
        startForeground(1, note);
        return START_STICKY;
    }

    private void gadangFile(long i) {
        Intent intent = new Intent("gadang");
        intent.putExtra("ukuranfile", i);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void prosesE(int i) {
        Intent intent = new Intent("sagiko");
        intent.putExtra("progressValue", i);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void summ(short i) {
        Intent intent = new Intent("summIntent");
        intent.putExtra("summValue", i);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void readsizes(int i) {
        Intent intent = new Intent("readsizesIntent");
        intent.putExtra("readsizesValue", i);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void apoerrorE(int i) {
        Intent intent = new Intent("salah");
        intent.putExtra("error", i);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void start() {
        if (mIsRecording) {
            return;
        }
        new Thread() {
           // @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
                int minBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, CHANNEL, FORMAT);
                try {
                    mBuffer = new short[minBufferSize];   //QUIT AFTER FIRST FRESH LAUNCH
                    if (minBufferSize < 0) {
                        if (mHandler != null) {
                            mHandler.sendEmptyMessage(MSG_ERROR_GET_MIN_BUFFERSIZE);
                        }
                        return;
                    }
                    audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, CHANNEL, FORMAT, minBufferSize * 2);
                    if( Build.VERSION.SDK_INT== Build.VERSION_CODES.JELLY_BEAN){
                        NoiseSuppressor.create(audioRecord.getAudioSessionId());
                    }
                } catch (Exception e) {
                    apoerrorE(Constant.FRESH_LAUNCING_FAIL);
                    return;
                }

                if (audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {

                    apoerrorE(Constant.UNINITIALIZED);
                    return;
                }
                short[] buffer = new short[SAMPLE_RATE * (16 / 8)];
                byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];
                FileOutputStream output = null;
                File f = null;
                try {
                    onTracking(JUDUL);
                    output = new FileOutputStream(new File(JUDUL), true);
                    f = new File(JUDUL);
                } catch (FileNotFoundException e) {
                    apoerrorE(Constant.FILE_NOT_FOUND);
                    return;
                }

                SimpleLame.init(SAMPLE_RATE, 2, SAMPLE_RATE, BIT_RATE, QUALITY, GAIN);
                mIsRecording = true;
                try {
                    try {
                        audioRecord.startRecording();
                    } catch (IllegalStateException e) {

                        if (mHandler != null) {
                           /* onTracking("Ndak bisa mulai do");*/
                        }
                        return;
                    }
                    try {
                        int readSize = 0;
                        sum = 0;
                        while (mIsRecording) {
                            sum = 0;

                            readSize = audioRecord.read(buffer, 0, mBuffer.length);
                            if (readSize == 0) {
                                stops();
                                apoerrorE(Constant.READ_SIZE_EQUAL_ZERO);
                                return;
                            }
                            for (int i = 0; i < readSize; i++) {
                                sum += buffer[i] * buffer[i];
                            }
                            if (readSize < 0) {
                                break;
                            } else {
                                for (int i = 0; i < readSize; i++) {
                                    buffer[i] = (short) (buffer[i] * (boostSet + 1));
                                }

                                final double amplitude = sum / readSize;
                                ampli = (int) Math.sqrt(amplitude);

                                prosesE(ampli);
                                if (ampli < noiseSet * 100) {
                                    int max = buffer[0];
                                    for (int i = 0; i < readSize; i++) {
                                        buffer[i] = 0;

                                    }
                                }
                                if (!new File(JUDUL).exists()) {
                                    apoerrorE(Constant.FILE_NOT_FOUND);
                                    break;
                                }

                                int encResult = SimpleLame.encode(buffer, buffer, readSize, mp3buffer);
                                readsizes(buffer.length);
                                if (encResult < 0) {
                                    break;
                                }
                                StatFs statfs = new StatFs(size);
                                if (encResult != 0 && statfs.getBlockSize() > 1000) {
                                    try {
                                        output.write(mp3buffer, 0, encResult);
                                        Long l = f.length();
                                        gadangFile(l);
                                    } catch (IOException e) {

                                        break;
                                    }
                                } else {
                                    apoerrorE(Constant.STOP_CAUSED_LIMITED_BY_SIZE);
                                }
                            }
                        }
                        prosesE(0);
                        summ((short) 0);
                        readsizes(0);
                        int flushResult = SimpleLame.flush(mp3buffer);
                        if (flushResult < 0) {
                        }
                        if (flushResult != 0) {
                            try {
                                output.write(mp3buffer, 0, flushResult);
                            } catch (IOException e) {

                            }
                        }
                        try {
                            output.close();
                        } catch (IOException e) {

                            if (mHandler != null) {
                            }
                        }
                    } finally {
                        audioRecord.stop();
                        audioRecord.release();
                        if (audioRecord.getState() != AudioRecord.STATE_UNINITIALIZED) {
                        }
                    }
                } finally {
                    SimpleLame.close();
                    mIsRecording = false;
                }
                if (mHandler != null) {
                }
            }
        }.start();
    }

    public void stops() {
        mIsRecording = false;
    }

    public boolean isRecording() {
        return mIsRecording;
    }

    public void setHandle(Handler handler) {
        this.mHandler = handler;
    }

    int random(int max, int min) {
        int a;
        a = (int) (Math.random() * (max - min));
        return a;
    }

    @Override
    public void onCreate() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(boostValue, new IntentFilter("boostIntent"));
        /*onTracking("onCreate");*/

    }

    @Override
    public void onDestroy() {
        stops();
        stopForeground(true);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(boostValue);
        if (cdh != null) {
            if (cdh.MisStart) {
                cdh.stop();
            }
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        /*onTracking("onBind");*/
        return mMessenger.getBinder();
    }

    public void onTracking(String keterangan) {
        Log.d("Dari Servis", keterangan);
    }

    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    @Override
    public void onRebind(Intent intent) {
    }

    void logs(String message) {
        Log.v("Service ", message);
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(@NotNull final Message msg) {
            if (msg.what == 0) {/*onTracking("REKAM");*/
                Bundle data = msg.getData();
                JUDUL = data.getString("JUDUL");
                BIT_RATE = data.getInt("BIT_RATE", 0);
                SAMPLE_RATE = data.getInt("SAMPLE_RATE", 0);
                CHANNEL = data.getInt("CHANNEL", 0);
                FORMAT = data.getInt("FORMAT", 0);
                GAIN = data.getInt("GAIN", 0);
                start();
                updateNotif(getApplicationContext(), JUDUL, Constant.RECORDING_IN_PROGRESS);
                flagCDH = 1;
                try {
                    cdh.start();
                } catch (Exception e) {
                    cdh = new CallDetectHelper(RecordingService.this);
                    cdh.start();
                }
            } else if (msg.what == 1) {
                stops();
                updateNotif(getApplicationContext(), JUDUL, Constant.STOPPING);
                flagCDH = 0;
            } else if (msg.what == 2) {
                updateNotif(getApplicationContext(), JUDUL, Constant.PAUSING);
                stops();
                flagCDH = 0;
            }
        }
    }

    private NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

    private int notifID = 1;
    private boolean freshLaunch = true;
    RemoteViews expandedView;
    NotificationManager notificationManager;
    Intent notificationIntent;
    private void updateNotif(Context context, String message, int kind) {
        if (freshLaunch) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.setAction(Intent.ACTION_MAIN);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            expandedView = new RemoteViews(this.getPackageName(), R.layout.sdrec_notification);
            builder.setSmallIcon(R.drawable.small_icon).setContentTitle("Skipday Rec").setContentIntent(intent);
            expandedView.setTextViewText(R.id.idTitle, JUDUL);
            freshLaunch = false;
        }
        switch (kind) {
            case Constant.PAUSING: {
                expandedView.setViewVisibility(R.id.buttonJeda, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.buttonRecord, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.buttonresume, View.VISIBLE);
                expandedView.setViewVisibility(R.id.buttonStop, View.VISIBLE);
                expandedView.setViewVisibility(R.id.recordNotif, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.pausedNotif, View.VISIBLE);
                expandedView.setOnClickPendingIntent(R.id.buttonresume, PendingIntent.getBroadcast(this, 0, new Intent("ACTION").putExtra("actions", Constant.NOTIF_RESUME), PendingIntent.FLAG_UPDATE_CURRENT));
                expandedView.setOnClickPendingIntent(R.id.buttonStop, PendingIntent.getBroadcast(this, 0, new Intent("STOP").putExtra("actions", Constant.NOTIF_STOP), PendingIntent.FLAG_UPDATE_CURRENT));
            }
            break;
            case Constant.RECORDING_IN_PROGRESS: {
                expandedView.setViewVisibility(R.id.buttonJeda, View.VISIBLE);
                expandedView.setViewVisibility(R.id.buttonRecord, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.buttonresume, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.buttonStop, View.VISIBLE);
                expandedView.setViewVisibility(R.id.recordNotif, View.VISIBLE);
                expandedView.setViewVisibility(R.id.pausedNotif, View.INVISIBLE);
                expandedView.setTextViewText(R.id.idStat,message.substring(message.lastIndexOf('/')+1,message.length()));
                expandedView.setOnClickPendingIntent(R.id.buttonJeda, PendingIntent.getBroadcast(this, 0, new Intent("ACTION").putExtra("actions", Constant.NOTIF_PAUSE), PendingIntent.FLAG_UPDATE_CURRENT));
                expandedView.setOnClickPendingIntent(R.id.buttonStop, PendingIntent.getBroadcast(this, 0, new Intent("STOP").putExtra("actions", Constant.NOTIF_STOP), PendingIntent.FLAG_UPDATE_CURRENT));
            }
            break;
            case Constant.STOPPING: {
                expandedView.setViewVisibility(R.id.buttonRecord, View.VISIBLE);
                expandedView.setViewVisibility(R.id.buttonresume, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.buttonStop, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.buttonJeda, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.recordNotif, View.INVISIBLE);
                expandedView.setViewVisibility(R.id.pausedNotif, View.INVISIBLE);
                expandedView.setTextViewText(R.id.idStat,"Standby");
                expandedView.setOnClickPendingIntent(R.id.buttonRecord, PendingIntent.getBroadcast(this, 0, new Intent("ACTION").putExtra("actions", Constant.NOTIF_RECORD), PendingIntent.FLAG_UPDATE_CURRENT));
            }
            break;
        }
        builder.setContent(expandedView);
        notificationManager.notify(notifID, builder.build());
    }
}
