package com.skipday.sdrec.lite.TabContent;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.skipday.sdrec.lite.R;
import com.skipday.sdrec.lite.TabContent.PengaturanContent.SetSavePath;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RecordingsList extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener, MediaPlayer.OnPreparedListener {
    private static final String TAG = "DialogActivity";
    private static final int SINGLE_MODE = 0;
    private static final int MULTIPLE_CHOICE_MODE = 1;
    private final ArrayList<String> ar = new ArrayList<String>();
    private final ArrayList<String> arOfUnchecked = new ArrayList<String>();
    private final StringBuffer sb = new StringBuffer();
    public static List<RowDetail> songs;

    static ArrayAdapter<RowDetail> adapter;
    SharedPreferences prefs;
    int durasiInLong;
    private int tandai;
    @Nullable
    private
    String shareMultiSingle;
    @NotNull
    private
    ArrayList<Uri> filesToShare = new ArrayList<Uri>();
    private int tandaimulti;
    @Nullable
    private MediaPlayer mediaPlayer;
    private String value;
    private int flag;
    private Handler handler;
    private int checkdestroy;/*
    private ImageButton selectNone, delete;*/
    @NotNull
    private Boolean isChanging = false;
    private int multiplex;
    private static ListView listView;
    private TextView current, selection, lokasi, durasi, recordingPlaying, lokConst;
    private static TextView total;
    private SeekBar seekBar;
    @Nullable
    private Timer mtTimer;
    private static RecordingsList list = null;

    public static RecordingsList getInstance() {
        if (list == null) {
            list = new RecordingsList();
        }
        return list;
    }

    @Nullable
    private final BroadcastReceiver nowPosReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            int currPositionIntent;
            currPositionIntent = intent.getIntExtra("currPosition", 0);
            current.setText("" + msToString(currPositionIntent));
            if (mediaPlayer != null && !mediaPlayer.
                    isPlaying()
                    &&
                    checkdestroy == 0
                    &&
                    mtTimer != null) {
                ImageButton pause = (ImageButton) rootView.findViewById(R.id.pause);
                pause.setVisibility(View.INVISIBLE);
            }
        }
    };
    View rootView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.list_recording, container, false);
        long satu = System.currentTimeMillis();
        listView = (ListView) rootView.findViewById(android.R.id.list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiper);
        current = (TextView) rootView.findViewById(R.id.currDur);
        durasi = (TextView) rootView.findViewById(R.id.maxDur);
        seekBar = (SeekBar) rootView.findViewById(R.id.seekBarSongs);
        seekBar.setOnSeekBarChangeListener(new seekbar());
        //selectAll = (ImageButton) rootView.findViewById(R.id.piliahsado);
        /*selectNone = (ImageButton) rootView.findViewById(R.id.selectnone);
        delete = (ImageButton) rootView.findViewById(R.id.deleteCheckedItemsButton);*/
        total = (TextView) rootView.findViewById(R.id.total);
        //shareBanyak = (ImageButton) rootView.findViewById(R.id.shareMulti);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(this);/*
        delete.setOnClickListener(this);
        selectNone.setOnClickListener(this);
        selectAll.setOnClickListener(this);
        shareBanyak.setOnClickListener(this);*/
        rootView.findViewById(R.id.pause).setOnClickListener(this);
        rootView.findViewById(R.id.resumee).setOnClickListener(this);
        long dua = System.currentTimeMillis();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(nowPosReceiver, new IntentFilter("currPositionIntent"));
        songs = new ArrayList<RowDetail>();
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
        long tiga = System.currentTimeMillis();
        new loadFiles(3).execute();
        long empat = System.currentTimeMillis();

        return rootView;
    }

    private String folderRec, lokasli, lok;

    @NotNull
    private static String msToString(long ms) {
        long totalSecs = ms / 1000;
        long hours = (totalSecs / 3600);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00" : ((mins < 10) ? "0" + mins : "" + mins);
        String secsString = (secs == 0)
                ? "00" : ((secs < 10) ? "0" + secs : "" + secs);
        if (hours > 0)
            return hours + ":" + minsString + ":" + secsString;
        else if (mins > 0)
            return mins + ":" + secsString;
        else return "0:" + secsString;
    }


    @NotNull
    static String readableFileSize(long size) {
        if (size <= 0)
            return "0";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    void toastTampil(String toast) {
        Toast.makeText(getActivity(), toast, Toast.LENGTH_SHORT).show();
    }

    static String dateModif(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy HH:mm");
        return formatter.format(new Date(date));
    }

    private void checkNow(boolean i) {
        Intent intent = new Intent("CHECK_INTENT");
        intent.putExtra("CHECK_VALUE", i);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public static void addList(String name) {
        RowDetail detail = new RowDetail();
        detail.nama = new File(name).getName();
        detail.ukuran = "Size: " + readableFileSize(new File(name).length());
        detail.created =dateModif(new File(name).lastModified());
        songs.add(detail);
        total.setText(""+songs.size());
        adapter.notifyDataSetChanged();
    }

    private class loadFiles extends AsyncTask<Void, Void, Void> {
        private int kind;

        protected loadFiles(int what) {
            this.kind = what;
        }

        private TextView status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new File(folderRec + "/").mkdir();
            status = (TextView) rootView.findViewById(R.id.recordplaying);
            status.setText("Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            File f = new File(folderRec + "/");

            //noinspection ResultOfMethodCallIgnored

            f.mkdir();
            final File[] files = f.listFiles(
                    new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            return pathname.toString().toLowerCase().endsWith(".mp3") && pathname.length() > 0;
                        }
                    }
            );
            switch (kind) {
                case 0: {
                    sortNameAsc(files);
                }
                break;
                case 1: {
                    sortNameDsc(files);
                }
                break;
                case 2: {
                    sortDateAsc(files);
                }
                break;
                case 3: {
                    sortDateDsc(files);
                }
                break;
                case 4: {
                    sortSizeAsc(files);
                }
                break;
                case 5: {
                    sortSizeDsc(files);
                }
                break;
            }

            assert files != null;
            final String[] theNamesOfFiles = new String[files.length];

            songs.clear();
            for (int i = 0; i < theNamesOfFiles.length; i++) {
                RowDetail detail = new RowDetail();
                detail.nama = files[theNamesOfFiles.length - i - 1].getName();
                detail.ukuran = "Size: " + readableFileSize(files[theNamesOfFiles.length - i - 1].length());
                detail.created = dateModif(files[theNamesOfFiles.length - i - 1].lastModified());
                songs.add(detail);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            status.setText("");
            try {
                refresh();

            }catch (Exception e){}

            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater menuInflater = getActivity().getMenuInflater();
            menuInflater.inflate(R.menu.cab_menu, menu);
            mode.setTitle(ar.size() + " selected");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.all: {
                    selectAll();
                }
                break;
                case R.id.share: {
                    SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
                    assert checkedItems != null;
                    final int checkedItemsCount = checkedItems.size();
                    if (checkedItemsCount == 1) {
                        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                            shareSingle(shareMultiSingle);
                        } else {
                            toastTampil("File not found");
                        }

                    } else {
                        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                            shareMulti();
                        } else {
                            toastTampil("File not found");
                        }

                    }
                }
                break;
                case R.id.delete: {
                    deleteSelectedItems();
                }
                break;
                case R.id.none: {
                    clearSelection();
                    singleSelect();
                    actionMode.finish();
                }
                break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            clearSelection();
            singleSelect();
        }
    };

    void sortNameAsc(File[] files) {
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(@NotNull File f1, @NotNull File f2) {
                return String.CASE_INSENSITIVE_ORDER.compare(f2.getName(), f1.getName());
            }
        });
    }

    void sortNameDsc(File[] files) {
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(@NotNull File f1, @NotNull File f2) {
                return String.CASE_INSENSITIVE_ORDER.compare(f1.getName(), f2.getName());
            }
        });
    }

    void sortDateAsc(File[] files) {
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(@NotNull File f1, @NotNull File f2) {
                return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
            }
        });
    }

    void sortDateDsc(File[] files) {
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(@NotNull File f1, @NotNull File f2) {
                return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
            }
        });
    }

    void sortSizeAsc(File[] files) {
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(@NotNull File f1, @NotNull File f2) {
                return Long.valueOf(f2.length()).compareTo(f1.length());
            }
        });
    }

    void sortSizeDsc(File[] files) {
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(@NotNull File f1, @NotNull File f2) {
                return Long.valueOf(f1.length()).compareTo(f2.length());
            }
        });
    }

    void siapkanAdapter() {
        adapter = new ArrayAdapter<RowDetail>(getActivity(), R.layout.item, songs) {
            @Nullable
            @Override
            public View getView(int position, @Nullable View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.item, null);
                }
                final View row = convertView;
                View check = row.findViewById(R.id.itemCheckBox);
                View hapus = row.findViewById(R.id.apuih);
                View rename = row.findViewById(R.id.namoulang);
                View soundinfo = row.findViewById(R.id.soundinfo);
                View openWith=row.findViewById(R.id.open_with);
                View deletethis = row.findViewById(R.id.deleteThis);
                TextView nama = (TextView) row.findViewById(R.id.nama);
                TextView ukuran = (TextView) row.findViewById(R.id.ukuran);
                TextView created = (TextView) row.findViewById(R.id.created);
                nama.setText(songs.get(position).nama);
                ukuran.setText(songs.get(position).ukuran);
                created.setText(songs.get(position).created);
                if (tandaimulti == 1) {
                    check.setVisibility(View.VISIBLE);
                    YoYo.with(Techniques.FadeOut).playOn(rename);
                    rename.setVisibility(View.INVISIBLE);
                    hapus.setVisibility(View.INVISIBLE);
                } else {
                    check.setVisibility(View.INVISIBLE);
                    YoYo.with(Techniques.FadeIn).playOn(rename);
                    rename.setVisibility(View.VISIBLE);
                    hapus.setVisibility(View.VISIBLE);
                }
                String itemInThisRow = songs.get(position).nama;
                hapus.setTag(position);

                hapus.setOnClickListener(RecordingsList.this);
                rename.setTag(itemInThisRow);
                rename.setOnClickListener(RecordingsList.this);
                soundinfo.setTag(itemInThisRow);
                soundinfo.setOnClickListener(RecordingsList.this);
                deletethis.setTag(position);
                deletethis.setOnClickListener(RecordingsList.this);
                openWith.setTag(itemInThisRow);
                openWith.setOnClickListener(RecordingsList.this);
                return convertView;
            }

        };
        listView.setAdapter(adapter);
        addClickListener();
        listView.setItemsCanFocus(false);
        buatsingle();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.a_z: {
                new loadFiles(0).execute();
            }
            break;
            case R.id.z_a: {
                new loadFiles(1).execute();
            }
            break;
            case R.id.oldest: {
                new loadFiles(2).execute();
            }
            break;
            case R.id.newest: {
                new loadFiles(3).execute();
            }
            break;
            case R.id.smallest: {
                new loadFiles(4).execute();
            }
            break;
            case R.id.largest: {
                new loadFiles(5).execute();

            }
            break;

        }
        flag = SINGLE_MODE;
        total.setVisibility(View.VISIBLE);
        total.setText("" + songs.size());
        return super.onOptionsItemSelected(item);
    }

    void buatsingle() {
        lokConst = (TextView) rootView.findViewById(R.id.lokConst);
        lokasi = (TextView) rootView.findViewById(R.id.judulLokasi);
        //selection = (TextView) findViewById(R.id.selection);
        lokasi.setVisibility(View.VISIBLE);
        lokConst.setText("Location: ");/*
        selectAll.setVisibility(View.GONE);
        shareBanyak.setVisibility(View.GONE);
        selectNone.setVisibility(View.INVISIBLE);
        delete.setVisibility(View.INVISIBLE);*/
    }

    int random(int max, int min) {
        int a;
        a = (int) (Math.random() * (max - min));
        return a;
    }

    @Override
    public void onResume() {
        long lima = System.currentTimeMillis();
        super.onResume();

        String M_BACKGROUND = prefs.getString("background", "fgh");
        Boolean ata = prefs.getBoolean("ata", false);
        int a = prefs.getInt("alpha", random(255, 0));
        int r = prefs.getInt("red", random(255, 0));
        int g = prefs.getInt("green", random(255, 0));
        int b = prefs.getInt("blue", random(255, 0));

        long enam = System.currentTimeMillis();
        assert mediaPlayer != null;
        if (flag == SINGLE_MODE && !mediaPlayer.isPlaying()) {
            if (mediaPlayer.getCurrentPosition() != 0) {
                mediaPlayer.reset();
            }
            folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
            String storageState = Environment.getExternalStorageState();
            if (!storageState.equals(null) && storageState.equals(Environment.MEDIA_MOUNTED)) {
                lokasi = (TextView) rootView.findViewById(R.id.judulLokasi);

                lokasli = folderRec + "/";
                lok = TextUtils.substring(folderRec, 1, TextUtils.lastIndexOf(lokasli, '/'));
                lokasi.setSelected(true);
                lokasi.setText(lok);

                mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new loadFiles(3).execute();
                    }
                });
                lokasi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivityForResult(new Intent(getActivity(), SetSavePath.class), SetSavePath.PICK_DIRECTORY);
                    }
                });
                //total.setText("" + listView.getCount());

            } else toastTampil("Storage not ready");
        }
        long tujuh = System.currentTimeMillis();
        /*if (ata) {
            if (M_BACKGROUND.equals("SKIPDAY")) {
                rootView.findViewById(R.id.swiperefresh).setBackgroundColor(Color.argb(a, r, g, b));

                //findViewById(R.id.headset).setBackgroundResource(Color.argb(0, 0, 0, 0));
            } else {
                Resources res = getResources();
                Bitmap bitmap = BitmapFactory.decodeFile(M_BACKGROUND);
                BitmapDrawable bd = new BitmapDrawable(res, bitmap);
                rootView.findViewById(R.id.swiperefresh).setBackgroundDrawable(bd);
            }

        } else {
            rootView.findViewById(R.id.swiperefresh).setBackgroundColor(Color.argb(0, 0, 0, 0));
        }*/
        mSwipeRefreshLayout.setColorScheme(R.color.color_scheme_1_1, R.color.color_scheme_1_2,
                R.color.color_scheme_1_3, R.color.color_scheme_1_4);
        //YoYo.with(Techniques.FadeIn).duration(1500).playOn(listView);
        long lapan = System.currentTimeMillis();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @NotNull Intent data) {
        if (resultCode != -1) return;
        if (requestCode == SetSavePath.PICK_DIRECTORY && resultCode == -1) {
            Bundle extras = data.getExtras();
            String path = (String) extras.get(SetSavePath.CHOSEN_DIRECTORY);
            SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putString("directory", path);
            editor.apply();
            folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
            lokasli = folderRec + "/";
            lok = TextUtils.substring(folderRec, 1, TextUtils.lastIndexOf(lokasli, '/'));
            lokasi.setSelected(true);
            lokasi.setText(lok);
        }
    }

    void refresh() {
        flag = SINGLE_MODE;
        checkNow(true);
        siapkanAdapter();
        singleSelect();

        total.setVisibility(View.VISIBLE);
        total.setText("" + listView.getCount());
        listView.setEnabled(true);
        listView.setSelector(R.drawable.recordings_list_row);
        seekBar.setProgress(0);
        current.setText("");
        durasi.setText("");
        try {
            actionMode.finish();
        }catch (Exception e){}
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }
        try{
            extendedM.setVisibility(View.INVISIBLE);
            generalR.setVisibility(View.VISIBLE);
            extendedM=null;
            generalR=null;
        }catch (Exception e){}
    }

    @SuppressWarnings("deprecation")
    public void onClick(@NotNull View v) {
        ImageButton resume = (ImageButton) rootView.findViewById(R.id.resumee);
        ImageButton pause = (ImageButton) rootView.findViewById(R.id.pause);
        switch (v.getId()) {
            case R.id.pause:
                assert mediaPlayer != null;
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    resume.setVisibility(View.VISIBLE);
                    pause.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.resumee:

                if (multiplex == 1) {
                    assert mediaPlayer != null;
                    mediaPlayer.start();
                    resume.setVisibility(View.INVISIBLE);
                    pause.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getActivity(), "Select recording to play", Toast.LENGTH_SHORT).show();
                }

                break;/*
            case R.id.close:
                View extendedM = listView.getChildAt((Integer) v.getTag() - listView.getFirstVisiblePosition()).findViewById(R.id.extendedMenu);
                View generalR = listView.getChildAt((Integer) v.getTag() - listView.getFirstVisiblePosition()).findViewById(R.id.generalRow);
                extendedM.setVisibility(View.INVISIBLE);
                YoYo.with(Techniques.StandUp).playOn(generalR);
                generalR.setVisibility(View.VISIBLE);
                listView.setEnabled(true);
                break;*/
            case R.id.deleteThis:
                listView.setItemChecked((Integer) v.getTag(), true);
                deleteSelectedItems();

                listView.setEnabled(true);
                break;
            /*case R.id.soundinfo:
                Intent intent = new Intent(RecordingsList.this, SongInfo.class);
                intent.putExtra("filename", v.getTag().toString());
                startActivity(intent);
                break;*/
            case R.id.apuih:
                if (flag == SINGLE_MODE) {
                    shareMultiSingle = null;
                    listView.setItemChecked((Integer) v.getTag(), true);
                    shareMultiSingle = songs.get((Integer) v.getTag()).nama;
                    itemSelection();
                    for (int i = 0; i < ar.size(); i++) {
                        assert shareMultiSingle != null;
                        if (shareMultiSingle.equals(ar.get(i))) {
                            YoYo.with(Techniques.BounceIn).duration(300).playOn(listView.getChildAt((Integer) v.getTag() - listView.getFirstVisiblePosition()).findViewById(R.id.itemCheckBox));
                        }
                    }


                }
                toggleChoiceMode();
                break;
            case R.id.namoulang:
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    renameDialog((String) v.getTag());
                } else {
                    toastTampil("File not found");
                }
                break;
            case R.id.open_with:{
                Intent intent=new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                File music = new File(folderRec + "/" + (String) v.getTag());
                intent.setDataAndType(Uri.fromFile(music), "audio/*");
                startActivity(intent);
            }break;
        }
    }

    void shareSingle(String fileToShares) {
        File e;
        e = new File(folderRec + "/" + fileToShares);
        Uri uri = Uri.fromFile(e);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("*/*");
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share " + fileToShares));
    }

    void shareMulti() {
        Intent share = new Intent(Intent.ACTION_SEND_MULTIPLE);
        share.setType("*/*");
        share.putParcelableArrayListExtra(Intent.EXTRA_STREAM, filesToShare);
        if (filesToShare.size() == 1) {
            startActivity(Intent.createChooser(share, "Share 1 recording"));
        } else {
            startActivity(Intent.createChooser(share, "Share " + filesToShare.size() + " recordings"));
        }
    }

    void singleSelect() {
        tandaimulti = 0;
        flag = SINGLE_MODE;
        clearSelection();
        buatsingle();
        YoYo.with(Techniques.StandUp).playOn(total);
        total.setVisibility(View.VISIBLE);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lokasi.setVisibility(View.VISIBLE);

    }

    private void toggleChoiceMode() {
        flag = MULTIPLE_CHOICE_MODE;
/*
        YoYo.with(Techniques.StandUp).playOn(selectAll);
        YoYo.with(Techniques.StandUp).playOn(shareBanyak);
        YoYo.with(Techniques.StandUp).playOn(selectNone);
        YoYo.with(Techniques.StandUp).playOn(delete);
        selectAll.setVisibility(View.VISIBLE);
        shareBanyak.setVisibility(View.VISIBLE);
        selectNone.setVisibility(View.VISIBLE);
        delete.setVisibility(View.VISIBLE);*/

        lokConst = (TextView) rootView.findViewById(R.id.lokConst);
        tandaimulti = 1;
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        actionMode = listView.startActionMode(actionModeCallback);
    }

    private ActionMode actionMode;

    void itemSelection() {
        File e;
        SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
        assert checkedItems != null;
        final int checkedItemsCount = checkedItems.size();
        ar.clear();
        filesToShare.clear();
        sb.delete(0, sb.capacity());
        for (int i = 0; i < checkedItemsCount; ++i) {
            final int position = checkedItems.keyAt(i);
            final boolean isChecked = checkedItems.valueAt(i);
            if (isChecked) {
                ar.add(songs.get(position).nama);
                e = new File(folderRec + "/" + songs.get(position).nama);
                Uri uri = Uri.fromFile(e);
                filesToShare.add(uri);
                sb.append(songs.get(position).nama).append("\n");

            }
        }
    }

    void deleteSelectedItems() {
        itemSelection();
        if (ar.size() != 0) {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                hapusBanyak();
            } else {
                toastTampil("File not found");
            }

        } else {
            Toast.makeText(getActivity(), "Please select item to delete", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearSelection() {
        final int itemCount = listView.getCount();
        flag = SINGLE_MODE;
        for (int i = 0; i < itemCount; i++) {
            listView.setItemChecked(i, false);


        }
    }

    private void selectAll() {
        File e;
        // tracking("here");
        ar.clear();
        filesToShare.clear();
        final int itemCount = listView.getCount();
        for (int i = 0; i < itemCount; i++) {
            listView.setItemChecked(i, true);
            e = new File(folderRec + "/" + songs.get(i).nama);
            Uri uri = Uri.fromFile(e);
            filesToShare.add(uri);
            ar.add(songs.get(i).nama);
        }
        lokConst.setText(itemCount + " recordings selected");
        lokasi.setVisibility(View.GONE);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @NotNull
    private Dialog renameDialog(@NotNull final String gantiNamas) {
        final EditText input = new EditText(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Enter new name: ");
        builder.setView(input);
        input.setText(gantiNamas);
        input.setSelection(0, TextUtils.lastIndexOf(gantiNamas, '.'));
        input.setFilters(filter());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //noinspection ConstantConditions
                value = input.getText().toString();
                File lama = new File(folderRec + "/" + gantiNamas);
                File baru = new File(folderRec + "/" + value);
                //noinspection ResultOfMethodCallIgnored
                lama.renameTo(baru);
                checkNow(true);
                new loadFiles(3).execute();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //new loadFiles(3).execute();
            }
        });
        return builder.show();
    }

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    private Dialog hapusBanyak() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Delete Files");
        if (ar.size() == 1) {
            builder.setMessage(Html.fromHtml("Delete<font color=#cc7832> " + ar.get(0) + "</font> ?"));
        } else if (ar.size() == listView.getCount()) {
            builder.setMessage(Html.fromHtml("Delete all (<b>" + ar.size() + "</b>) recordings ?"));
        } else {
            builder.setMessage("Delete " + ar.size() + " recordings?");
        }
        Log.d("ASDf", String.valueOf(ar.size()));
        builder.setIcon(android.R.drawable.stat_sys_warning);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.d("qwe", "ASDFASDF");

                final ProgressDialog deleting = new ProgressDialog(getActivity());
                deleting.setMax(ar.size());
                deleting.setTitle("Deleting...");
                deleting.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                deleting.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < ar.size(); i++) {
                            new File(folderRec + "/" + ar.get(i)).delete();
                            deleting.setProgress(i);
                        }
                        checkNow(true);
                        handler.sendEmptyMessage(0);
                    }

                }).start();
                handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        try {
                            deleting.dismiss();
                            new loadFiles(3).execute();
                            if(mediaPlayer.isPlaying()){
                                mediaPlayer.stop();
                            }
                            try {
                                actionMode.finish();
                            }catch (Exception e){
                            }

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                };

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //listView.setEnabled(false);
            }
        });
        builder.setNeutralButton("List file", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Files to be delete")
                        .setMessage(sb).setPositiveButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hapusBanyak();
                    }
                }).show();
            }
        });
        return builder.show();
    }

    @NotNull
    InputFilter[] filter() {
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Nullable
            @Override
            public CharSequence filter(@NotNull CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    char[] karakterTerlarang = new char[]{',', '=', '}'};
                    for (int index = start; index < end; index++) {
                        if (new String(karakterTerlarang).contains(String.valueOf(source.charAt(index)))) {
                            return "";
                        }
                    }
                }
                return null;
            }

        };
        return filters;
    }

    private void addClickListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (flag == MULTIPLE_CHOICE_MODE) {
                    for (int i = 0; i < ar.size(); i++) {
                        if (ar.get(i).equals(songs.get(position).nama)) {
                            YoYo.with(Techniques.BounceIn).duration(300).playOn(view.findViewById(R.id.itemCheckBox));
                        }
                    }
                    itemSelection();
                    for (int i = 0; i < ar.size(); i++) {
                        if (ar.get(i).equals(songs.get(position).nama)) {
                            YoYo.with(Techniques.BounceIn).duration(300).playOn(view.findViewById(R.id.itemCheckBox));
                        }
                    }

                    if (ar.size() == 0) {
                        lokConst.setText("");
                        lokasi.setVisibility(View.GONE);
                        tandai = 1;
                        singleSelect();
                        actionMode.finish();
                    }
                    actionMode.setTitle(ar.size() + " selected");
                }
                if (flag == SINGLE_MODE && tandai == 0) {
                    if(extendedM!=null){
                        return;
                    }
                    multiplex = 1;

                    recordingPlaying = (TextView) rootView.findViewById(R.id.recordplaying);
                    recordingPlaying.setSelected(true);
                    ImageButton resume = (ImageButton) rootView.findViewById(R.id.resumee);
                    ImageButton pause = (ImageButton) rootView.findViewById(R.id.pause);
                    resume.setVisibility(View.INVISIBLE);
                    pause.setVisibility(View.VISIBLE);
                    String musics = songs.get(position).nama;
                    recordingPlaying.setText(musics);
                    assert mediaPlayer != null;
                    mediaPlayer.reset();
                    try {
                        mediaPlayer.setDataSource(folderRec + "/" + musics);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        Log.e(TAG, "Could not open file " +
                                musics + " for playback.", e);
                    }
                    durasi.setText("" + msToString(mediaPlayer.getDuration()));
                    mediaPlayer.start();
                    seekBar.setMax(mediaPlayer.getDuration());

                    mtTimer = new Timer();

                    TimerTask mTimertask = new TimerTask() {
                        @Override
                        public void run() {
                            if (isChanging) {
                                return;
                            }
                            if (mtTimer == null) {
                                return;
                            }
                            if (mtTimer != null && mediaPlayer != null && mediaPlayer.isPlaying()) {
                                nowPos(mediaPlayer.getCurrentPosition());
                                seekBar.setProgress(mediaPlayer.getCurrentPosition());
                            }
                        }
                    };
                    if (mtTimer == null) {
                        return;
                    }
                    if (mtTimer != null && mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mtTimer.schedule(mTimertask, 0, 100);
                    }
                }
                tandai = 0;
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                try{
                    extendedM.setVisibility(View.INVISIBLE);
                    generalR.setVisibility(View.VISIBLE);
                    extendedM=null;
                    generalR=null;
                }catch (Exception e){}
                extendedM = listView.getChildAt(position - listView.getFirstVisiblePosition()).findViewById(R.id.extendedMenu);
                generalR = listView.getChildAt(position - listView.getFirstVisiblePosition()).findViewById(R.id.generalRow);
                View rowView = listView.getChildAt(position - listView.getFirstVisiblePosition());
                YoYo.with(Techniques.StandUp).playOn(extendedM);
                extendedM.setVisibility(View.VISIBLE);
                generalR.setVisibility(View.GONE);

                /*if (!generalR.isShown()) {
                    listView.setEnabled(false);
                }*/
                return false;
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                try{
                    extendedM.setVisibility(View.INVISIBLE);
                    generalR.setVisibility(View.VISIBLE);
                    extendedM=null;
                    generalR=null;
                }catch (Exception e){}

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    View extendedM;
    View generalR;
    private void nowPos(int i) {
        Intent intent = new Intent("currPositionIntent");
        intent.putExtra("currPosition", i);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        checkdestroy = 1;
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(nowPosReceiver);
        assert mediaPlayer != null;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }

        /*if (adViewListRecording2 != null) {
            adViewListRecording2.destroy();
        }*/
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;
        mtTimer = null;
        super.onDestroy();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
    }

    /*@Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }*/

    private class seekbar implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(@NotNull SeekBar seekBar, int progress, boolean fromUser) {
            current.setText("" + msToString(seekBar.getProgress()));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            isChanging = true;
        }

        @Override
        public void onStopTrackingTouch(@NotNull SeekBar seekBar) {
            assert mediaPlayer != null;
            mediaPlayer.seekTo(seekBar.getProgress());
            isChanging = false;
        }
    }
}
