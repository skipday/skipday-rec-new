/*
package com.skipday.sdrec.lite.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.skipday.sdrec.lite.helper.DBAdapter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.sql.SQLException;

*/
/**
 * Created by Jaga My Priera on 19/01/2015.
 *//*

public class contentProviderService extends Service {

    private final DBAdapter db = new DBAdapter(this);
    private boolean startCounting;
    private final BroadcastReceiver checkForTheSongs_dude = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            boolean shouldCheck = intent.getBooleanExtra("CHECK_VALUE", false);
            startCounting = shouldCheck;

            if (shouldCheck) {
                //commitNewsongs();
                Log.d("Should I perform a check? " + this, startCounting + "Nothing Happened :P");
            }


        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(checkForTheSongs_dude, new IntentFilter("CHECK_INTENT"));

    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(checkForTheSongs_dude);
    }

    void commitNewsongs() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                final String folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/SDRec/");
                File f = new File(folderRec + "/");
                try {
                    db.open();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                f.mkdir();
                final File[] files = f.listFiles();
                assert files != null;
                final String[] theNamesOfFiles = new String[files.length];
                if (db.isEmpty()) {

                    for (int i = 0; i < theNamesOfFiles.length; i++) {
                        if (files[theNamesOfFiles.length - i - 1].getName().endsWith(".mp3") && (files[theNamesOfFiles.length - i - 1].length() != 0)) {
                            db.insertTitle(files[theNamesOfFiles.length - i - 1].getName(), files[i].getAbsolutePath(), files[theNamesOfFiles.length - i - 1].length(), files[theNamesOfFiles.length - i - 1].lastModified());
                        }
                    }
                }

                if (!db.isEmpty()) {
                    Cursor cursor = null;
                    cursor = db.getAllColumn(DBAdapter.BY_NAME_INC);
                    if (cursor.moveToFirst()) {
                        do {
                            if (!new File(folderRec + "/" + cursor.getString(1)).exists()) {
                                db.deletesongs(Long.parseLong(cursor.getString(0)));
                            }
                        } while (cursor.moveToNext());
                    }

                    for (int i = 0; i < theNamesOfFiles.length; i++) {
                        if (!db.isExist(files[theNamesOfFiles.length - i - 1].getName())) {
                            if (files[theNamesOfFiles.length - i - 1].length() > 0
                                    && files[theNamesOfFiles.length - i - 1].getName().endsWith(".mp3")) {
                                db.insertTitle(files[theNamesOfFiles.length - i - 1].getName(), files[i].getAbsolutePath(), files[theNamesOfFiles.length - i - 1].length(), files[theNamesOfFiles.length - i - 1].lastModified());
                            }
                        }

                    }
                }
                db.close();
            }
        }).start();
    }
}
*/
