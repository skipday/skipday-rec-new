package com.skipday.sdrec.lite.TabContent;

/**
 * Created by jagamypriera on 18/07/15.
 */
public class Constant {
    public static final int UNINITIALIZED=1;
    public static final int FILE_NOT_FOUND=2;
    public static final int FRESH_LAUNCING_FAIL=3;
    public static final int READ_SIZE_EQUAL_ZERO=4;
    public static final int STOP_CAUSED_LIMITED_BY_SIZE=5;
    public static final int RECORDING_IN_PROGRESS=6;
    public static final int STOPPING=12;
    public static final int PAUSING=7;
    public static final int NOTIF_STOP=8;
    public static final int NOTIF_PAUSE=9;
    public static final int NOTIF_RECORD=10;
    public static final int NOTIF_RESUME=11;
}
