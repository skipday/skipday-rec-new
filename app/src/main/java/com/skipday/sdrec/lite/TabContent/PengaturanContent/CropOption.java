package com.skipday.sdrec.lite.TabContent.PengaturanContent;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by Jaga My Priera on 11/06/2014.
 */
public class CropOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
