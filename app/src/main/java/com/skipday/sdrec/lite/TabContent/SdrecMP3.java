package com.skipday.sdrec.lite.TabContent;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.skipday.sdrec.lite.CustomView.AnimatedView;
import com.skipday.sdrec.lite.R;
import com.skipday.sdrec.lite.TabContent.RecordingListContent.dialog;
import com.skipday.sdrec.lite.Services.RecordingService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SdrecMP3 extends android.support.v4.app.Fragment {
    private boolean freshLaunch=true;
    public static Handler handlerFBroadcast;
    @Nullable
    private static String jam = null, PREFIX;
    private final File lokasi = new File(Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec");

    private static SdrecMP3 sdrec = null;

    public static SdrecMP3 getInstance() {
        if (sdrec == null) {
            sdrec = new SdrecMP3();
        }
        return sdrec;
    }

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.main, container, false);
        long satu=System.currentTimeMillis();
        ads = (AdView) rootView.findViewById(R.id.adView);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(gadangFileReceiver, new IntentFilter("gadang"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(prosesEReceiver, new IntentFilter("sagiko"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(apoErrorE, new IntentFilter("salah"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(callDetect, new IntentFilter("adaPanggilanIntent"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(summReceiver, new IntentFilter("summIntent"));
        long dua=System.currentTimeMillis();
        loudnessToggle = (CheckBox) rootView.findViewById(R.id.boostSwitch);
        loudnessHelpButton = (Button) rootView.findViewById(R.id.boostHelp);
        anim = (AnimatedView) rootView.findViewById(R.id.anim_view);
        pauseButton = (Button) rootView.findViewById(R.id.PauseButton);
        currentBoost = (TextView) rootView.findViewById(R.id.currentBoost);
        startButton = (Button) rootView.findViewById(R.id.StartButton);
        stopButton = (Button) rootView.findViewById(R.id.StopButton);
        resumeButton = (Button) rootView.findViewById(R.id.ResumeButton);
        boost = (SeekBar) rootView.findViewById(R.id.boostSeekbar);
        onScreenSettings = (Button) rootView.findViewById(R.id.List);
        start = (ImageView) rootView.findViewById(R.id.starticon);
        currentFileSize = (TextView) rootView.findViewById(R.id.filesize);
        recordingTime = (Chronometer) rootView.findViewById(R.id.chronometer);
        currentFileSize2 = (TextView) rootView.findViewById(R.id.filesizes);
        paused = (ImageView) rootView.findViewById(R.id.pausedicon);
        stopped = (ImageView) rootView.findViewById(R.id.stooppedicon);
        textView=(TextView)rootView.findViewById(R.id.textView);
        //judul = (TextView) rootView.findViewById(R.id.judul);
        final long tiga=System.currentTimeMillis();
        boost.setEnabled(false);
        loudnessHelpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastHelp("Increase loudness");
                //dialog(2, "Multiply sounds volume.<br><font color=#cc7832>Warning: Don't set too high, can cause damage to your speaker</font>", getActivity());
            }
        });
        recordingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int which = random(3);
                switch (which) {
                    case 1: {
                        YoYo.with(Techniques.Tada).duration(1000).playOn(recordingTime);
                    }
                    case 2: {
                        YoYo.with(Techniques.RubberBand).duration(1000).playOn(recordingTime);
                    }
                    default: {
                        YoYo.with(Techniques.RubberBand).duration(1000).playOn(recordingTime);
                    }
                }


            }
        });
        loudnessToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    boost.setProgress(0);
                    boost.setEnabled(false);
                    setBoost(boost.getProgress());
                }
                if (isChecked) {
                    boost.setEnabled(true);
                }
            }
        });
        boost.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int boostV = boost.getProgress() + 1;
                setBoost(boost.getProgress());
                currentBoost.setText("" + boostV + "X");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        boolean mboolean;
        long empat=System.currentTimeMillis();
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mboolean = prefs.getBoolean("firstcheck", false);
        if (!mboolean) {
            getValidSampleRates();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstcheck", true);
            editor.putString("donList", "Donation history:\n");
            editor.putInt("alpha", random(255));
            editor.putInt("red", random(255));
            editor.putInt("green", random(255));
            editor.putInt("blue", random(255));
            editor.putString("prefix", "SKIPDAY");
            editor.putString("background", "SKIPDAY");
            editor.putString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec");
            editor.apply();
            lokasi.mkdir();
        }
        long lima=System.currentTimeMillis();

        long enam=System.currentTimeMillis();
        final Intent SERVICEINTENT = new Intent(getActivity(), RecordingService.class);
        if (SERVICEINTENT != null) {
            getActivity().bindService(
                    SERVICEINTENT,
                    mConnection,
                    Context.BIND_AUTO_CREATE);
        }
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(freshLaunch){
                    anim.setRADIUS(recordingTime.getWidth()/2+(recordingTime.getWidth()/6));
                    freshLaunch=false;

                }
                File state = new File(String.valueOf(Environment.getExternalStorageDirectory()));
                if (state.canWrite()) {
                    if (RECORDED == 0) {
                        assert PREFIX != null;
                        if (PREFIX.equals("SKIPDAY")) {
                            jam = new SimpleDateFormat("EEE MMM dd yyyy_HH mm ss", Locale.getDefault()).format(new Date());
                        } else {
                            String time = String.valueOf(System.currentTimeMillis());
                            jam = PREFIX + time;
                        }
                        //judul.setText(jam + ".mp3");
                        //NAMABARU = "" + judul.getText();
                        NAMABARU = jam + ".mp3";
                        FILE_NAME = M_LOCATION + "/" + jam + ".mp3";
                    }
                    stopButton.setEnabled(true);
                    REKAM(FILE_NAME,M_BITRATE,M_SAMPLERATE,M_CHANNEL,M_FORMAT,M_GAIN);
                    RECORDED = 1;
                    pauseButtonHasPressed = 1;
                    recordingTime.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
                    recordingTime.start();
                    tombolstarttekan();
                } else {
                    dialog(0, "Can't start recording. Check your SD card", getActivity());
                }
            }
        });
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tombolpausetekan();

            }
        });
        resumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tombolresumetekan();
            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                standby();

            }
        });
        onScreenSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (localFlag == 1) {
                    YoYo.with(Techniques.FlipOutX).duration(900).playOn(rootView.findViewById(R.id.noiseGateLayout));
                    localFlag = 0;
                } else {
                    YoYo.with(Techniques.FlipInX).duration(900).playOn(rootView.findViewById(R.id.noiseGateLayout));
                    rootView.findViewById(R.id.noiseGateLayout).setVisibility(View.VISIBLE);
                    localFlag = 1;
                }
            }
        });
        rootView.findViewById(R.id.controlleru).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (localFlag == 1) {
                    YoYo.with(Techniques.FlipOutX).duration(900).playOn(rootView.findViewById(R.id.noiseGateLayout));
                    localFlag = 0;
                } else {
                    YoYo.with(Techniques.FlipInX).duration(900).playOn(rootView.findViewById(R.id.noiseGateLayout));
                    rootView.findViewById(R.id.noiseGateLayout).setVisibility(View.VISIBLE);
                    localFlag = 1;
                }
            }
        });
        handlerFBroadcast = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.arg1) {
                    case Constant.NOTIF_PAUSE:
                        tombolpausetekan();
                        Log.d(getClass().getSimpleName(),"REEEEEEEE");
                        break;
                    case Constant.NOTIF_RESUME:
                        tombolresumetekan();
                        break;
                    case Constant.NOTIF_STOP:
                        standby();
                        break;
                    case Constant.NOTIF_RECORD:
                        if(freshLaunch){
                            anim.setRADIUS(recordingTime.getWidth()/2+(recordingTime.getWidth()/6));
                            freshLaunch=false;
                        }
                        File state = new File(String.valueOf(Environment.getExternalStorageDirectory()));
                        if (state.canWrite()) {
                            if (RECORDED == 0) {
                                assert PREFIX != null;
                                if (PREFIX.equals("SKIPDAY")) {
                                    jam = new SimpleDateFormat("EEE MMM dd yyyy_HH mm ss", Locale.getDefault()).format(new Date());
                                } else {
                                    String time = String.valueOf(System.currentTimeMillis());
                                    jam = PREFIX + time;
                                }
                                //judul.setText(jam + ".mp3");
                                //NAMABARU = "" + judul.getText();
                                NAMABARU = jam + ".mp3";
                                FILE_NAME = M_LOCATION + "/" + jam + ".mp3";
                            }
                            Log.d(getClass().getSimpleName(),"RECOOOORD8");
                            stopButton.setEnabled(true);
                            REKAM(FILE_NAME,M_BITRATE,M_SAMPLERATE,M_CHANNEL,M_FORMAT,M_GAIN);
                            RECORDED = 1;
                            pauseButtonHasPressed = 1;
                            recordingTime.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
                            recordingTime.start();
                            tombolstarttekan();
                        } else {
                            dialog(0, "Can't start recording. Check your SD card", getActivity());
                        }
                        break;
                }
            }
        };
        long tujuh=System.currentTimeMillis();
        Log.d(getClass().getSimpleName(),"onCreateView");
        return rootView;
    }

    private final int PAUSED_STATE = 2;
    private final int RECORDING_STATE = 1;
    private final BroadcastReceiver summReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {

        }
    };
    private Boolean fileNotFound = false;
    private int localFlag = 0;
    private int M_SAMPLERATE;
    private int M_BITRATE;
    private int RECORDED;
    private int M_GAIN;
    private int M_CHANNEL;
    private int M_FORMAT;
    private int progressset;
    private int pauseButtonHasPressed;
    private final BroadcastReceiver gadangFileReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            long gadangFile;
            gadangFile = intent.getLongExtra("ukuranfile", 0);
            if (gadangFile == 0) {
                currentFileSize.setText("");
            } else {
                currentFileSize.setText("" + baraGadangE(gadangFile));
            }
        }
    };
    private String M_BACKGROUND;
    private String M_LOCATION;
    private String value;
    private String NAMABARU;
    private Boolean askName;
    @NotNull
    private Boolean stopDrawing = false;
    private SeekBar boost;
    private String FILE_NAME;
    private CheckBox loudnessToggle;
    private Button loudnessHelpButton;
    private AnimatedView anim;
    private TextView textView;
    private final BroadcastReceiver prosesEReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            progressset = intent.getIntExtra("progressValue", 0);
            textView.setText(""+progressset);
            int sendLoud = progressset / 100;
            //anim.setGate(sendLoud);
            if (!stopDrawing) {
                anim.setLoudness(
                        sendLoud - (sendLoud % 2),
                        sendLoud + (sendLoud / 5),
                        sendLoud + sendLoud / 5,
                        sendLoud + (sendLoud ^ sendLoud),
                        sendLoud,
                        sendLoud ^ 2);
            } else {
                anim.setLoudness(0, 0, 0, 0, 0, 0);
            }
        }
    };
    private long timeWhenStopped;
    private TextView judul,currentFileSize, currentFileSize2, currentBoost;
    @Nullable
    private Messenger mService = null;
    private boolean mBound;
    private Chronometer recordingTime;
    private Button pauseButton, startButton, stopButton, onScreenSettings, resumeButton;
    private ImageView start, paused, stopped;
    private final BroadcastReceiver apoErrorE = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            int errorE;

            errorE = intent.getIntExtra("error", 0);
            switch (errorE) {
                case Constant.UNINITIALIZED: {
                    Toast.makeText(getActivity(), "Oops! Something went wrong, plese try again", Toast.LENGTH_LONG).show();
                }
                break;
                case Constant.FRESH_LAUNCING_FAIL: {
                    Toast.makeText(getActivity(), "Oops! Something went wrong, plese try again", Toast.LENGTH_LONG).show();
                }
                break;
                case Constant.READ_SIZE_EQUAL_ZERO: {
                    dialog(0, "Audio input being used by another application<br><b><font color=#cc7832 size=12pt>(e.g. another recording application)</b></font><br>Try to close that application or restart your device", getActivity());
                }
                break;
                case Constant.STOP_CAUSED_LIMITED_BY_SIZE: {
                    dialog(0, "No space left in the " + Environment.getExternalStorageDirectory() + " free up the storage and try again", getActivity());
                }
                break;
                case Constant.FILE_NOT_FOUND: {
                    dialog(0, "File not found", getActivity());
                    fileNotFound=true;
                }
                break;
                default:
            }
            stopCauseError();
        }
    };
    private final BroadcastReceiver callDetect = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            String perintah;
            perintah = intent.getStringExtra("adaPanggilan");
            if (RECORDED == 1 && pauseButtonHasPressed == 1) {
                if (perintah.equals("JEDA")) {
                    RECORDED = 1;
                    pauseButtonHasPressed = 0;
                    File f = new File(FILE_NAME);
                    Long i = f.length();
                    currentFileSize2.setText("" + baraGadangE(i));
                    JEDA();
                    timeWhenStopped = recordingTime.getBase() - SystemClock.elapsedRealtime();
                    //tracking("Pause Ditekan");
                    recordingTime.stop();
                    tombolpausetekan();

                }
            }

        }
    };
    @Nullable
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;

            //tracking("onServiceDisconnected");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
            BERHENTI();
            if (pauseButtonHasPressed == 1) {
                tombolstarttekan();
            }
        }
    };

    private void setBoost(int i) {
        Intent intent = new Intent("boostIntent");
        intent.putExtra("boostValue", i);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void stopCauseError() {
        File hapus;
        hapus = new File(FILE_NAME);
        BERHENTI();
        if (hapus.exists()) {
            hapus.delete();
        }
        standby();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().startService(new Intent(getActivity(), RecordingService.class));
        getActivity().bindService(new Intent(getActivity(), RecordingService.class), mConnection, Context.BIND_AUTO_CREATE);
        Log.d(getClass().getSimpleName(),"onCreate");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.pengaturan, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pengaturan: {
                startActivityForResult(new Intent(getActivity(), Pengaturan.class), 2);
            }
            break;
            case R.id.exit: {
                getActivity().stopService(new Intent(getActivity(), RecordingService.class));
                System.exit(0);
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    private AdView ads;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (ads != null) {
            ads.destroy();
        }
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(gadangFileReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(prosesEReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(apoErrorE);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(callDetect);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(summReceiver);
        Intent running = new Intent(getActivity(), RecordingService.class);
        if (mBound) {
            getActivity().unbindService(mConnection);//todo parent
            //tracking("UnBindingInOnDestroy");
            BERHENTI();
            getActivity().stopService(running);
            //tracking("onDestroy");
            mBound = false;
        }


    }

    private int random(int max) {
        int a;
        a = (int) (Math.random() * (max - 0));
        return a;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (ads != null) {
            ads.resume();
        }else {
            ads.setAdSize(AdSize.SMART_BANNER);
            AdRequest adRequest = new AdRequest.Builder().build();
            ads.loadAd(adRequest);
        }
        askName = prefs.getBoolean("askName", false);
        PREFIX = prefs.getString("prefix", "SKIPDAY");
        M_LOCATION = prefs.getString("directory", lokasi.toString());
        M_BITRATE = Integer.parseInt(prefs.getString("bitratePref", "128"));
        M_GAIN = 0;
        M_SAMPLERATE = prefs.getInt("SAMPLERATE", 0);
        M_CHANNEL = prefs.getInt("CHANNEL", 0);
        M_FORMAT = prefs.getInt("FORMAT", 0);
        M_BACKGROUND = prefs.getString("background", "fgh");
        int a = prefs.getInt("alpha", random(255));
        int r = prefs.getInt("red", random(255));
        int g = prefs.getInt("green", random(255));
        int b = prefs.getInt("blue", random(255));
        /*if (M_BACKGROUND.equals("SKIPDAY")) {
            rootView.findViewById(R.id.controller).setBackgroundColor(Color.argb(a, r, g, b));
        } else {
            Resources res = getResources();
            Bitmap bitmap = BitmapFactory.decodeFile(M_BACKGROUND);
            BitmapDrawable bd = new BitmapDrawable(res, bitmap);
            rootView.findViewById(R.id.controller).setBackgroundDrawable(bd);
        }*/

        new File(M_LOCATION).mkdir();
        if (RECORDED == 1) {
            File checkavailable = new File(FILE_NAME);
            if (!checkavailable.exists()) {
                Toast.makeText(getActivity(), "File not found: " + FILE_NAME + ", please check your SD card", Toast.LENGTH_LONG).show();
                fileNotFound = true;
                standby();
                fileNotFound = false;
            }
        }
        Log.d(getClass().getSimpleName(),"onRsume");
    }

    private void getValidSampleRates() {
        /*int sRate[]={ 96000, 44100, 22050, 16000, 11025,8000};*/
        int sRate[] = {8000, 11025, 16000, 22050, 44100, 48000};
        int configs[] = {AudioFormat.CHANNEL_IN_STEREO, AudioFormat.CHANNEL_IN_MONO};
        int formats[] = {AudioFormat.ENCODING_PCM_16BIT, AudioFormat.ENCODING_PCM_8BIT};
        int sampleRate;
        int channel_config;
        int format;
        int validsampleRate = -1;
        int validchannel_config = -1;
        int validformat = -1;
        int validBuffsize = -1;
        int buffsize;
        for (int x = 0; x < formats.length; ++x) {
            format = formats[x];
            for (int y = 0; y < sRate.length; ++y) {
                sampleRate = sRate[y];
                for (int z = 0; z < configs.length; ++z) {
                    channel_config = configs[z];

                    buffsize = AudioRecord.getMinBufferSize(sampleRate, channel_config, format);
                    if (buffsize > 0) {
                        validsampleRate = sampleRate;
                        validchannel_config = channel_config;
                        validformat = format;
                        validBuffsize = buffsize;
                    }

                }
            }
        }
        Log.d("Information from Main Activity. Valid config is-> Format :", validformat + " Sample rate: " + validsampleRate + " Channel: " + validchannel_config + " Buffer size: " + validBuffsize);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("SAMPLERATE", validsampleRate);
        editor.putInt("FORMAT", validformat);
        editor.putInt("CHANNEL", validchannel_config);
        editor.apply();
    }

    private void toastHelp(String hint) {
        Toast.makeText(getActivity(), hint, Toast.LENGTH_SHORT).show();
    }


    @NotNull
    static Dialog dialog(int tandai, String pesan, @NotNull Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //builder.setIcon(android.R.drawable.stat_sys_warning);
        if (tandai == 1) {
            builder.setTitle("Thank you :)");
        }
        if (tandai == 2) {
            builder.setTitle("Help me");
        }
        builder.setMessage(Html.fromHtml(pesan));
        builder.setPositiveButton("I got it", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(@NotNull DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.show();

    }

private void REKAM(String judul, int bitrate, int sampleRate, int channel, int format, int gain){
    if (!mBound)
        return;
    Message msg = Message.obtain(null, RecordingService.REKAM);
    try {
        assert mService != null;
        Bundle data=new Bundle();
        data.putString("JUDUL", judul);
        data.putInt("BIT_RATE", bitrate);
        data.putInt("SAMPLE_RATE", sampleRate);
        data.putInt("CHANNEL", channel);
        data.putInt("FORMAT", format);
        data.putInt("GAIN", gain);
        msg.setData(data);
        mService.send(msg);
    } catch (RemoteException e) {
        e.printStackTrace();
    }
    pauseButtonHasPressed = 1;
}

    private void BERHENTI() {
        if (!mBound)
            return;
        Message msg = Message.obtain(null, RecordingService.BERHENTI);
        try {
            assert mService != null;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void JEDA() {
        if (!mBound)
            return;
        Message msg = Message.obtain(null, RecordingService.JEDA);
        try {
            assert mService != null;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        pauseButtonHasPressed = 0;
    }

    @NotNull
    private String baraGadangE(long size) {
        if (size <= 0)
            return "0";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,###.00").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private void tombolresumetekan() {
        RECORDED = 1;
        pauseButtonHasPressed = 1;
        REKAM(FILE_NAME,M_BITRATE,M_SAMPLERATE,M_CHANNEL,M_FORMAT,M_GAIN);
        recordingTime.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
        recordingTime.start();
        stopDrawing = false;
        /*toastHelp("(SDRec) Resume recording");*/
        resumeButton.setVisibility(View.INVISIBLE);
        pauseButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.VISIBLE);
        start.setVisibility(View.VISIBLE);
        stopped.setVisibility(View.INVISIBLE);
        paused.setVisibility(View.INVISIBLE);
        currentFileSize.setVisibility(View.VISIBLE);
        currentFileSize2.setVisibility(View.INVISIBLE);

        stopButton.setEnabled(true);
    }


    private void tombolstarttekan() {
        stopDrawing = false;
        /*toastHelp("(SDRec) Start recording");*/
        currentFileSize.setVisibility(View.VISIBLE);
        currentFileSize2.setVisibility(View.INVISIBLE);
        resumeButton.setVisibility(View.GONE);
        startButton.setVisibility(View.INVISIBLE);
        pauseButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.VISIBLE);
        start.setVisibility(View.VISIBLE);
        stopped.setVisibility(View.INVISIBLE);
        paused.setVisibility(View.INVISIBLE);
        stopButton.setEnabled(true);
    }

    private void tombolpausetekan() {
        /*toastHelp("(SDRec) Pause");*/
        RECORDED = 1;
        pauseButtonHasPressed = 0;
        File f = new File(FILE_NAME);
        Long i = f.length();
        currentFileSize2.setText("" + baraGadangE(i));
        JEDA();
        timeWhenStopped = recordingTime.getBase() - SystemClock.elapsedRealtime();
        recordingTime.stop();
        anim.setLoudness(0, 0, 0, 0, 0, 0);
        currentFileSize.setVisibility(View.INVISIBLE);
        currentFileSize2.setVisibility(View.VISIBLE);
        startButton.setVisibility(View.INVISIBLE);
        pauseButton.setVisibility(View.GONE);
        resumeButton.setVisibility(View.VISIBLE);
        start.setVisibility(View.INVISIBLE);
        stopped.setVisibility(View.INVISIBLE);
        paused.setVisibility(View.VISIBLE);
        stopButton.setEnabled(true);
    }

    private void standby() {
        RECORDED = 0;
        pauseButtonHasPressed = 0;
        recordingTime.setBase(SystemClock.elapsedRealtime());
        recordingTime.stop();
        timeWhenStopped = 0;
        //judul.setText("Standby");
        tombolstoptekan();
        BERHENTI();
        Toast.makeText(getActivity(),"Saved as "+jam+".mp3",Toast.LENGTH_SHORT).show();//todo
        if(!askName && !fileNotFound){
            RecordingsList.addList(FILE_NAME);
        }

        if (askName && !fileNotFound) {
            new dialog().rename(NAMABARU, getActivity(), "RENAME");
            fileNotFound = false;
        }
    }

    private void tombolstoptekan() {
        stopDrawing = true;
        anim.setLoudness(0, 0, 0, 0, 0, 0);
        stopButton.setEnabled(false);
        pauseButton.setVisibility(View.INVISIBLE);
        resumeButton.setVisibility(View.INVISIBLE);
        startButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.VISIBLE);
        start.setVisibility(View.INVISIBLE);
        stopped.setVisibility(View.VISIBLE);
        paused.setVisibility(View.INVISIBLE);
        currentFileSize.setText("");
        currentFileSize2.setText("");
        currentFileSize.setVisibility(View.INVISIBLE);
        currentFileSize2.setVisibility(View.VISIBLE);

    }


    @NotNull
    private InputFilter[] filter() {

        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Nullable
            @Override
            public CharSequence filter(@NotNull CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    char[] karakterTerlarang = new char[]{',', '=', '}'};
                    for (int index = start; index < end; index++) {
                        if (new String(karakterTerlarang).contains(String.valueOf(source.charAt(index)))) {
                            return "";
                        }
                    }
                }
                return null;
            }

        };
        return filters;
    }

    private SharedPreferences prefs;

    @NotNull
    private Dialog rename(@NotNull final String gantiNamas) {

        final String folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
        final EditText input = new EditText(getActivity());
        //tracking("createExampleDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Rename " + gantiNamas);
        builder.setMessage("Enter new name: ");
        builder.setIcon(R.drawable.rename_dialog);
        builder.setView(input);
        input.setText(gantiNamas);
        input.setSelection(0, TextUtils.lastIndexOf(gantiNamas, '.'));
        input.setFilters(filter());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //noinspection ConstantConditions
                value = input.getText().toString();
                File lama = new File(folderRec + "/" + gantiNamas);
                File baru = new File(folderRec + "/" + value);
                //noinspection ResultOfMethodCallIgnored
                lama.renameTo(baru);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //tracking("negative refresh");
            }
        });
        builder.setNeutralButton("Delete this file", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                File lama = new File(folderRec + "/" + gantiNamas);
                lama.delete();
            }
        });
        return builder.show();
    }


}