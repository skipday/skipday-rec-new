package com.skipday.sdrec.lite.TabContent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

/**
 * Created by SKIPDAY on 21/03/2015.
 */
public class Broadcast extends BroadcastReceiver {
    protected final static int RECORDING = 1, PAUSING = 2;

    @Override
    public void onReceive(Context context, Intent intent) {
        Message msg = Message.obtain();
        msg.setTarget(SdrecMP3.handlerFBroadcast);
        Log.d(getClass().getSimpleName(),"asdfasdfasdfa"+intent.getIntExtra("actions", 0));
        switch (intent.getIntExtra("actions", 0)) {
            case Constant.NOTIF_STOP:
                msg.arg1 = Constant.NOTIF_STOP;
                msg.sendToTarget();
                break;
            case Constant.NOTIF_PAUSE:
                msg.arg1 = Constant.NOTIF_PAUSE;
                msg.sendToTarget();
                break;
            case Constant.NOTIF_RECORD:
                msg.arg1 = Constant.NOTIF_RECORD;
                msg.sendToTarget();
                break;
            case Constant.NOTIF_RESUME:
                msg.arg1 = Constant.NOTIF_RESUME;
                msg.sendToTarget();
                break;
        }

    }
}
