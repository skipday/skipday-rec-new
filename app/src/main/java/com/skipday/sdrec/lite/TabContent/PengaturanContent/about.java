/*
package com.skipday.sdrec.lite.tabContent.pengaturanContent;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.skipday.sdrec.lite.R;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

*/
/**
 * Created by Jaga My Priera on 19/06/2014.
 *//*

public class about extends ListActivity {
    private final String[] dari = {"name", "site", "developer", "license", "purpose"};
    private final int[] ke = {R.id.name, R.id.site, R.id.developer, R.id.license, R.id.purpose};
    private final ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
    private TextView version;
    @NotNull
    private
    HashMap<String, String> temp1;
    @NotNull
    private
    HashMap<String, String> temp2;
    @NotNull
    private HashMap<String, String> temp3 = new HashMap<String, String>();

    void aboutContent() {

        temp1 = new HashMap<String, String>();
        temp2 = new HashMap<String, String>();
        temp3 = new HashMap<String, String>();
        temp1.put("name", "Android Platform");
        temp1.put("site", "http://android.com");
        temp1.put("developer", "Open Handset Alliance / Google, Inc.");
        temp1.put("license", "Apache License");
        temp1.put("purpose", "Mobile Platform");
        list.add(temp1);
        temp2.put("name", "MPEG Audio Layer III (MP3) encoder");
        temp2.put("site", "http://lame.sourceforge.net");
        temp2.put("developer", "LAME Developers");
        temp2.put("license", "GNU Library General Public License");
        temp2.put("purpose", "Encoding audio");
        list.add(temp2);
        */
/*temp3.put("name", "Android View Animations");
        temp3.put("site", "http://github.com/daimajia/AndroidViewAnimations");
        temp3.put("developer", "Daimajia");
        temp3.put("license", "MIT License");
        temp3.put("purpose", "View Animation");
        list.add(temp3);*//*



    }

    public String dateModif(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy  HH:mm");
        return formatter.format(new Date(date));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about);
        PackageManager pm = this.getPackageManager();
        assert pm != null;
        ApplicationInfo installAt = null;
        PackageInfo versi = null;
        try {
            installAt = pm.getApplicationInfo(getPackageName(), 0);
            versi = pm.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert installAt != null;
        assert versi != null;
        String appFile = installAt.sourceDir, namaVersi = versi.versionName, kodeVersi = String.valueOf(versi.versionCode);

        long installed = new File(appFile).lastModified();
        TextView installedDate = (TextView) findViewById(R.id.iDate);
        ImageButton tips = (ImageButton) findViewById(R.id.tips);
        tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog(about.this);
            }
        });
        installedDate.setText("" + dateModif(installed));
        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), list, R.layout.itemabout, dari, ke);
        aboutContent();
        setListAdapter(adapter);
        version = (TextView) findViewById(R.id.version);
        version.setText("" + namaVersi + " (" + kodeVersi + ")");
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String M_BACKGROUND = prefs.getString("background", "fgh");
        Boolean ata = prefs.getBoolean("ata", false);
        int a = prefs.getInt("alpha", random(255, 0));
        int r = prefs.getInt("red", random(255, 0));
        int g = prefs.getInt("green", random(255, 0));
        int b = prefs.getInt("blue", random(255, 0));
        if (ata) {
            if (M_BACKGROUND.equals("SKIPDAY")) {
                findViewById(R.id.aboutly).setBackgroundColor(Color.argb(a, r, g, b));
            } else {
                Resources res = getResources();
                Bitmap bitmap = BitmapFactory.decodeFile(M_BACKGROUND);
                BitmapDrawable bd = new BitmapDrawable(res, bitmap);
                findViewById(R.id.aboutly).setBackgroundDrawable(bd);
            }

        } else {
            findViewById(R.id.aboutly).setBackgroundColor(Color.argb(255, 0, 0, 0));
        }
    }

    @NotNull
    Dialog dialog(@NotNull Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(Html.fromHtml("<font color=#cc7832>TIP:</font> long click a button to show up the hint"));
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("One tip");
        builder.setPositiveButton("I got it", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(@NotNull DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.show();
    }

    int random(int max, int min) {
        int a;
        a = (int) (Math.random() * (max - min));
        return a;
    }
}
*/
