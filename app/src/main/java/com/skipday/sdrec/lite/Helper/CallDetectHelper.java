package com.skipday.sdrec.lite.Helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Jaga My Priera on 17/06/2014.
 */
public class CallDetectHelper {
    private final Context ctx;
    private final CallStateListener callStateListener;
    private final OutgoingReceiver outgoingReceiver;
    @NotNull
    public Boolean MisStart = false;
    private TelephonyManager tm;

    public CallDetectHelper(Context ctx) {
        this.ctx = ctx;

        callStateListener = new CallStateListener();
        outgoingReceiver = new OutgoingReceiver();
    }

    private void adaPanggilan(String perintah) {
        Intent intent = new Intent("adaPanggilanIntent");
        intent.putExtra("adaPanggilan", perintah);
        LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);
    }

    /**
     * Start calls detection.
     */
    public void start() {
        MisStart = true;
        tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        ctx.registerReceiver(outgoingReceiver, intentFilter);
        logCTT("start");
    }

    /**
     * Stop calls detection.
     */
    public void stop() {
        MisStart = false;
        tm.listen(callStateListener, PhoneStateListener.LISTEN_NONE);
        ctx.unregisterReceiver(outgoingReceiver);
        logCTT("stop");
    }

    public boolean isStart() {
        return MisStart;
    }

    public void logCTT(String pesan) {
        Log.d("cDH ", pesan);
    }

    /**
     * Listener to detect incoming calls.
     */
    private class CallStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    // called when someone is ringing to this phone
                    adaPanggilan("JEDA");
                    break;
            }
        }
    }

    /**
     * Broadcast receiver to detect the outgoing calls.
     */
    public class OutgoingReceiver extends BroadcastReceiver {
        public OutgoingReceiver() {
        }

        @Override
        public void onReceive(Context context, @NotNull Intent intent) {
            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

            adaPanggilan("JEDA");
        }

    }
}
