/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skipday.sdrec.lite.TabContent.PengaturanContent;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.skipday.sdrec.lite.R;
import com.skipday.sdrec.lite.util.IabHelper;
import com.skipday.sdrec.lite.util.IabResult;
import com.skipday.sdrec.lite.util.Inventory;
import com.skipday.sdrec.lite.util.Purchase;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Donating {
    static final int TANK_MAX = 4;
    private static final String TAG = "Donate ";
    private static final String SKU_DONATE = "donate";
    private static final int RC_REQUEST = 10001;
    int mTank;
    private TextView donateList;

    // The helper object
    @Nullable
    private
    IabHelper mHelper;
    @NotNull
    private
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, @NotNull IabResult result) {
            if (mHelper == null) return;
        }
    };
    @NotNull
    private
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(@NotNull IabResult result, @NotNull Inventory inventory) {
            Log.d(TAG, result.getMessage());

            if (mHelper == null) return;
            Purchase donatePurchase = inventory.getPurchase(SKU_DONATE);
            if (donatePurchase != null) {
                Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_DONATE), mConsumeFinishedListener);
            }

        }
    };
    @NotNull
    private
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(@NotNull IabResult result, @NotNull Purchase purchase) {
            if (mHelper == null) return;
            try {
                if (purchase.getSku().equals(SKU_DONATE)) {
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
            }catch (Exception e){
                Log.d(getClass().getSimpleName(),""+e.getMessage());
            }

        }
    };
    private Context context;
public Donating(Context context){
    this.context=context;
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuOAsKQOjspx/Y0yJ8w+uquOnxcFF3u/R2ktpSnQref7wT/C0sest5QTG7FOc9CO/YdB0qXudambZRIW+QD7DhhBLUR6nOV55OOBCkAWtnqMk+Q+EojId2nDqjH+XO0vhtuSxA2Zqd21v3tpwBo5sIUvtTCJOJuoLf25ZUEebrGQZX4MHvgH8T0JplMRPN+qykYrrES2gq64nY9CvjeHmhtVrsDyrDSERDH273NptplhDddI30kNtLrI56qsL5cViRB6Zq1xGC8JNz1CQmywFpdyXdgKBQ8WQOuCwHkMrTqlIyUM+DLUwqejBZC7Y0yK2/5M40Vlt7PJCpKp9Y+ipMwIDAQAB";
    mHelper = new IabHelper(context, base64EncodedPublicKey);
    mHelper.enableDebugLogging(false);
    Log.d(TAG, "Starting setup.");
    mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
        public void onIabSetupFinished(@NotNull IabResult result) {
            Log.d(TAG, "Setup finished.");
            if (mHelper == null) return;
            Log.d(TAG, "Setup successful. Querying inventory.");
            mHelper.queryInventoryAsync(mGotInventoryListener);
        }
    });


}


public void transaction(){
    try {

        mHelper.launchPurchaseFlow((Activity) context, SKU_DONATE, RC_REQUEST,mPurchaseFinishedListener);
    }catch (Exception e){
        Log.e(TAG, e.getMessage());
    }
}
}
