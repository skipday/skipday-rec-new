package com.skipday.sdrec.lite;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.skipday.sdrec.lite.TabContent.RecordingsList;
import com.skipday.sdrec.lite.TabContent.SdrecMP3;

import java.util.Locale;

/**
 * Created by jagamypriera on 15/07/15.
 */
public class SectionsPageAdapter extends FragmentPagerAdapter {
    private Context context;
    public SectionsPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context=context;
    }
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:{
                fragment= SdrecMP3.getInstance();
            }break;
            case 1:{
                fragment= RecordingsList.getInstance();
            }break;
        }
        return fragment;
    }
    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return "Recorder";
            case 1:
                return "Recordings";
        }
        return null;
    }
}
