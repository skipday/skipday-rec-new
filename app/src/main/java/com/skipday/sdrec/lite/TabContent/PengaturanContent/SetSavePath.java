package com.skipday.sdrec.lite.TabContent.PengaturanContent;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.skipday.sdrec.lite.R;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jaga My Priera on 24/04/2014.
 */
public class SetSavePath extends ListActivity {

    public static final String START_DIR = "startDir";
    public static final String SHOW_HIDDEN = "showHidden";
    public static final String CHOSEN_DIRECTORY = "chosenDir";
    public static final int PICK_DIRECTORY = 2726;
    private static final String ONLY_DIRS = "onlyDirs";
    AdapterView.OnItemLongClickListener listener;
    private File dir;
    private boolean showHidden = false;
    private boolean onlyDirs = true;
    private String value;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
        Bundle extras = getIntent().getExtras();
        dir = Environment.getExternalStorageDirectory();
        if (extras != null) {
            String preferredStartDir = extras.getString(START_DIR);
            showHidden = extras.getBoolean(SHOW_HIDDEN, false);
            onlyDirs = extras.getBoolean(ONLY_DIRS, true);
            if (preferredStartDir != null) {
                File startDir = new File(preferredStartDir);
                if (startDir.isDirectory()) {
                    dir = startDir;
                }
            }
        }

        setContentView(R.layout.chooser_list);
        setTitle(dir.getAbsolutePath());
        final ListView lv = getListView();
        lv.setTextFilterEnabled(true);
        final ArrayList<File> files = filter(dir.listFiles(), onlyDirs, showHidden);
        String[] names = names(files);
        final ListAdapter listAdapter = new ArrayAdapter<String>(this, R.layout.list_item, names);
        setListAdapter(listAdapter);
        Button btnChoose = (Button) findViewById(R.id.btnChoose);
        Button toroot = (Button) findViewById(R.id.toroot);
        String name = dir.getName();
        longs();
        if (name.length() == 0)
            name = "/";
        toroot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (!files.get(position).isDirectory())
                    return;*/
                String path = "/";
                Intent intent = new Intent(SetSavePath.this, SetSavePath.class);
                intent.putExtra(SetSavePath.START_DIR, path);
                intent.putExtra(SetSavePath.SHOW_HIDDEN, showHidden);
                intent.putExtra(SetSavePath.ONLY_DIRS, onlyDirs);
                startActivityForResult(intent, PICK_DIRECTORY);
                Log.d("Asdfasfd", path);
            }
        });
        btnChoose.setText("Choose " + "'" + name + "'");
        btnChoose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                returnDir(dir.getAbsolutePath());

                Log.d("Dir ", dir.getAbsolutePath());
            }
        });
        Button newFolder = (Button) findViewById(R.id.newDir);
        newFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewFolder(dir.getAbsolutePath());

            }
        });


        if (!dir.canRead()) {
            Context context = getApplicationContext();
            String msg = "Could not read folder contents.";
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
            toast.show();
            return;
        }


        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!files.get(position).isDirectory())
                    return;
                String path = files.get(position).getAbsolutePath();
                Intent intent = new Intent(SetSavePath.this, SetSavePath.class);
                intent.putExtra(SetSavePath.START_DIR, path);
                intent.putExtra(SetSavePath.SHOW_HIDDEN, showHidden);
                intent.putExtra(SetSavePath.ONLY_DIRS, onlyDirs);
                startActivityForResult(intent, PICK_DIRECTORY);
                Log.d("Asdfasfd", path);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @NotNull Intent data) {
        if (requestCode == PICK_DIRECTORY && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            String path = (String) extras.get(SetSavePath.CHOSEN_DIRECTORY);
            returnDir(path);
        }
    }

    private void returnDir(String path) {
        Intent result = new Intent();
        result.putExtra(CHOSEN_DIRECTORY, path);
        setResult(RESULT_OK, result);
        finish();
    }

    @NotNull
    public ArrayList<File> filter(@NotNull File[] file_list, boolean onlyDirs, boolean showHidden) {
        ArrayList<File> files = new ArrayList<File>();
        for (File file : file_list) {
            if (onlyDirs && !file.isDirectory())
                continue;
            if (!showHidden && file.isHidden())
                continue;
            files.add(file);
        }
        Collections.sort(files);
        return files;
    }

    @NotNull
    public String[] names(@NotNull ArrayList<File> files) {
        String[] names = new String[files.size()];
        int i = 0;
        for (File file : files) {
            names[i] = file.getName();
            i++;
        }
        return names;
    }

    @NotNull
    private Dialog createNewFolder(final String folderBaru) {
        final EditText input = new EditText(getApplicationContext());
        AlertDialog.Builder builder = new AlertDialog.Builder(SetSavePath.this);

        builder.setTitle("Create New Folder");
        builder.setMessage("Enter folder name: ");
        builder.setIcon(R.drawable.rename_dialog);
        builder.setView(input);
        input.setText("New Folder");
        input.setTextColor(Color.BLACK);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //noinspection ConstantConditions
                value = input.getText().toString();
                File fb = new File(folderBaru + "/" + value);
                if (!fb.exists()) {
                    fb.mkdir();
                } else {
                    Toast.makeText(SetSavePath.this, "Folder " + value + " already exist", Toast.LENGTH_SHORT).show();
                }
                String path = folderBaru + "/" + value;
                Intent intent = new Intent(SetSavePath.this, SetSavePath.class);
                intent.putExtra(SetSavePath.START_DIR, path);
                intent.putExtra(SetSavePath.SHOW_HIDDEN, showHidden);
                intent.putExtra(SetSavePath.ONLY_DIRS, onlyDirs);
                startActivityForResult(intent, PICK_DIRECTORY);
                Log.d("Asdfasfd", path);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return builder.show();
    }

    public void longs() {
        listener = new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long id) {
                return false;
            }
        };


        getListView().setOnItemLongClickListener(listener);
    }
}

