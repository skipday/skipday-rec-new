package com.skipday.sdrec.lite.TabContent.RecordingListContent;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.skipday.sdrec.lite.R;
import com.skipday.sdrec.lite.TabContent.RecordingsList;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jaga My Priera on 18/06/2014.
 */
public class dialog extends Activity implements MediaPlayer.OnPreparedListener {
    private MediaPlayer mediaPlayer;

    @NotNull
    InputFilter[] filter() {
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Nullable
            @Override
            public CharSequence filter(@NotNull CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    char[] karakterTerlarang = new char[]{',', '=', '}'};
                    for (int index = start; index < end; index++) {
                        if (new String(karakterTerlarang).contains(String.valueOf(source.charAt(index)))) {
                            return "";
                        }
                    }
                }
                return null;
            }

        };
        return filters;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
    }

    @NotNull
    public Dialog rename(@NotNull final String gantiNamas, @NotNull Context context, @NotNull final String s) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final String folderRec = prefs.getString("directory", Environment.getExternalStorageDirectory().getPath() + "/Skipday Rec/");
        final SharedPreferences.Editor editor = prefs.edit();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_seek, null);
        builder.setIcon(R.drawable.rename_dialog);
        builder.setView(view);
        final EditText input = (EditText) view.findViewById(R.id.enterText);
        final ImageButton play = (ImageButton) view.findViewById(R.id.play);
        final ImageButton pause = (ImageButton) view.findViewById(R.id.pause);
        final TextView judul = (TextView) view.findViewById(R.id.qualityvalue);
        final SeekBar progress = (SeekBar) view.findViewById(R.id.quality);

        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(folderRec + "/" + gantiNamas);
            mediaPlayer.prepare();
        } catch (IOException ignored) {
        }
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.start();
                Log.d("asdfasdf", "play");
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.pause();
                Log.d("asdfasdf", "pause");
            }
        });
        input.setVisibility(View.VISIBLE);
        judul.setVisibility(View.GONE);
        if (s.equals("PREFIX")) {
            input.setHint(gantiNamas);
            progress.setVisibility(View.GONE);
            pause.setVisibility(View.GONE);
            play.setVisibility(View.GONE);
        }
        builder.setMessage("Enter new name: ");
        if (s.equals("RENAME")) {
            input.setText(gantiNamas);
            pause.setVisibility(View.VISIBLE);
            play.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            input.setSelection(0, TextUtils.lastIndexOf(gantiNamas, '.'));
        }
        input.setFilters(filter());

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                if (s.equals("RENAME")) {
                    //noinspection ConstantConditions

                    File lama = new File(folderRec + "/" + gantiNamas);
                    File baru = new File(folderRec + "/" + value);
                    //noinspection ResultOfMethodCallIgnored
                    lama.renameTo(baru);
                    RecordingsList.addList(folderRec + "/" + value);
                }
                if (s.equals("PREFIX")) {

                    editor.putString("prefix", value);
                    editor.apply();
                }
                mediaPlayer.reset();
                mediaPlayer.release();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mediaPlayer.reset();
                mediaPlayer.release();
                RecordingsList.addList(folderRec + "/" + gantiNamas);
            }
        });
        if (s.equals("RENAME")) {
            builder.setNeutralButton("Delete this file", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    File lama = new File(folderRec + "/" + gantiNamas);
                    lama.delete();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                }
            });
        }
        if (s.equals("PREFIX")) {
            builder.setNeutralButton("Default", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    editor.putString("prefix", "SKIPDAY");
                    editor.apply();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                }
            });
        }
        return builder.show();
    }
}
